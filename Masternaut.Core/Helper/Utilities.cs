﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Drawing;

namespace Masternaut.Core.Helper
{
    public static class Utilities
    {
        public static ResponseModel<object> SaveCropImage(string imgBase64String, string saveFilePath, string extension)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                imgBase64String = imgBase64String.Replace("data:image/png;base64,", "");

                byte[] bytes = Convert.FromBase64String(imgBase64String);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }
                if (extension.ToLower() != "png")
                    ImageOptimize.OptimizeSaveJpeg(saveFilePath, image, 90);
                else
                {
                    image.Save(saveFilePath);
                }
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Image uploaded ";
            }
            catch (Exception err)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = err.Message;
            }

            return mResult;
        }

        public static Language GetCurrentLanguage()
        {
            string selectedLang = System.Globalization.CultureInfo.CurrentCulture.ToString();
            var language = Language.English;
            switch (selectedLang)
            {
                case "en-GB":
                    language = Language.English;
                    break;
                case "nl-NL":
                    language = Language.Dutch;
                    break;
                case "fr-FR":
                    language = Language.French;
                    break;
                default:
                    language = Language.French;
                    break;
            }
            return language;
        }

        public static string StripHtml(string html)
        {
            var returnText = string.Empty;
            if (html == null || html == string.Empty)
                return string.Empty;
            var Text = System.Text.RegularExpressions.Regex.Replace(html, "<[^>]*>", string.Empty);
            if (Text.Length > 0)
            {
                if (Text.Length > 150)
                {
                    var takeWord = Text.Split().Take(20);
                    returnText = string.Join(" ", takeWord);
                }
                else
                    returnText = Text;
            }
            else
            {
                returnText = string.Empty;
            }
            return returnText + "....";
        }



    }
}
