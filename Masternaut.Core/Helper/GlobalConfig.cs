﻿using Masternaut.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Helper
{
    public class GlobalConfig
    {
        public const string ProjectName = "Masternaut";
        public const string DateFormat = "dddd dd MMM yyyy @ hh:mm tt";
        public const string DisplayDateOnlyFormat = "dd MMM yyyy";
        public const string DisplayTimeOnlyFormat = "hh:mm tt";
        public const string DisplayDateFormat = "dd MMM yyyy - hh:mm tt";


        public static string baseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static string baseRoutePath = ConfigurationManager.AppSettings["BaseRoutePath"];

        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string fileBaseUrl = baseUrl + "Files/";
        public static string DefaultfileBaseUrl = baseUrl + "Files/DefaultImages/";
        public static string BrochurefileBaseUrl = baseUrl + "Files/Brochure/";

        #region Email
        public static string EmailUserName = ConfigurationManager.AppSettings["EmailUserName"].ToString();
        public static string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
        public static string SMTPClient = ConfigurationManager.AppSettings["SMTPClient"].ToString();
        public static int SMTPPort = 26;
        #endregion

        public static string NewsImagePath = fileRoutePath + @"NewsImage\";
        public static string NewsImageUrl = fileBaseUrl + "NewsImage/";
        public static string NewsDefaultImage = "Default.jpg";

        public static string EventImagePath = fileRoutePath + @"EventImage\";
        public static string EventImageUrl = fileBaseUrl + "EventImage/";
        public static string EventDefaultImage = "Default.jpg";

        public static string StoriesImagePath = fileRoutePath + @"StoriesImage\";
        public static string StoriesImageUrl = fileBaseUrl + "StoriesImage/";
        public static string StoriesDefaultImage = "Default.jpg";

        public static string ResumePath = fileRoutePath + @"Resume\";
        public static string ResumeUrl = fileBaseUrl + "Resume/";

        public static int AdminPageSize = 10;

        public const int PageSize = 5;
        public const Language DefaultLanguageId = Language.English;
    }
}
