﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Helper
{
    public static class Utility
    {
        public static DateTime GetSystemDateTimeUTC()
        {
            return DateTime.UtcNow;
        }
    }
}
