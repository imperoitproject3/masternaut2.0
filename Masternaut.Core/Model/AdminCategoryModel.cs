﻿using Masternaut.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class CategoryDropdownModel
    {
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class AdminCategoryModel
    {
        public long Id { get; set; }

        public List<CategoryLanguageModel> Categorylanguagemodellist { get; set; }
    }

    public class CategoryLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please enter category name")]
        public string CategoryName { get; set; }

        public virtual string LanguageName { get; set; }
    }
}
