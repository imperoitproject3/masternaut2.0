﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class IndustryTypeModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long Count { get; set; }
    }
}
