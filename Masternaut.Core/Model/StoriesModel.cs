﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class StoriesAddUpdateModel
    {
        public StoriesAddUpdateModel()
        {
            imageName = GlobalConfig.NewsDefaultImage;
        }
        public long Id { get; set; }

        public long IndustryTypeId { get; set; }

        public string imageName { get; set; }

        public List<StoriesLanguageModel> StoriesLanguagemodellist { get; set; }
    }

    public class StoriesLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter description name")]
        public string Description { get; set; }

        public virtual string LanguageName { get; set; }
    }
    public class StoriesViewModel : StoriesAddUpdateModel
    {
        public string IndustryType { get; set; }
    }

    public class SingleStoriesViewModel
    {
        public SingleStoriesViewModel()
        {
            RelatedStories = new List<SingleStoriesViewModel>();
        }
        public long Id { get; set; }

        public long IndustryTypeId { get; set; }

        public string IndustryType { get; set; }

        public string ImageName { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<SingleStoriesViewModel> RelatedStories { get; set; }
    }

    public class CustomerStoriesViewModel
    {
        public CustomerStoriesViewModel()
        {
            IndustryType = new List<IndustryTypeModel>();
            Stories = new List<SingleStoriesViewModel>();
        }
        public List<IndustryTypeModel> IndustryType { get; set; }
        public List<SingleStoriesViewModel> Stories { get; set; }
    }
}
