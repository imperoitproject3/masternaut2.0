﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Masternaut.Core.Model
{
    public class NewsAddUpdateModel
    {
        public NewsAddUpdateModel()
        {
            imageName = GlobalConfig.NewsDefaultImage;
        }
        public long Id { get; set; }

        public string imageName { get; set; }

        public long CategoryId { get; set; }

        public List<NewsLanguageModel> Newslanguagemodellist { get; set; }

        public DateTime DateTime { get; set; } = DateTime.UtcNow;
    }

    public class NewsLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter description name")]
        public string Description { get; set; }

        public virtual string LanguageName { get; set; }
    }

    public class NewsViewModel : NewsAddUpdateModel
    {
        public string CategoryName { get; set; }
    }

    public class SingleNewsViewModel
    {
        public SingleNewsViewModel()
        {
            LatestNewsList = new List<NewsTitle>();
        }
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageName { get; set; }

        public DateTime CreatedDate { get; set; }

        public long NextRecordId { get; set; }

        public long NextRecordTitle { get; set; }

        public List<NewsTitle> LatestNewsList { get; set; }

    }
    public class NewsTitle
    {
        public long Id { get; set; }
        public string Title { get; set; }
    }
    public class ViewAllNewsModel
    {
        public decimal PageCount { get; set; }
        public int PageIndex { get; set; }
        public List<SingleNewsViewModel> NewsList { get; set; }
        public List<NewsTitle> LatestNewsList { get; set; }
    }
}
