﻿using Masternaut.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class AdminLoginModel
    {
        public AdminLoginModel()
        {
            Role = UserRoles.Admin;
            RememberMe = false;
        }

        [Required]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string EmailId { get; set; }

        [Required]
        [DisplayName("Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        public UserRoles Role { get; set; }

        public bool RememberMe { get; set; }
    }

    public class AdminChangePasswordModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string ConfirmPassword { get; set; }
    }

    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string Email { get; set; }
    }
}
