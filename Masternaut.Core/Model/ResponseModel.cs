﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class ResponseModel<T> where T : new()
    {
        public ResponseModel()
        {
            Result = new T();
            Status = ResponseStatus.Failed;
            Message = string.Empty;
        }

        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }

    public class PaginationModel
    {
        public PaginationModel()
        {
            PageIndex = 1;
            PageSize = GlobalConfig.PageSize;
        }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long Id { get; set; }
    }
}
