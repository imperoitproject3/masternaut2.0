﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class EventAddUpdateModel
    {
        public EventAddUpdateModel()
        {
            imageName = GlobalConfig.EventDefaultImage;
            StartTime = Utility.GetSystemDateTimeUTC();
            EndTIme = Utility.GetSystemDateTimeUTC().AddDays(2);
        }
        public long Id { get; set; }

        public string imageName { get; set; }

        [Required(ErrorMessage = "Please select EventDate")]
        public DateTime EventDate { get; set; }

        [Required(ErrorMessage = "Please select StartTime")]
        public DateTime StartTime { get; set; }

        [Required(ErrorMessage = "Please select EndTIme")]
        public DateTime EndTIme { get; set; }

        [Required(ErrorMessage = "Please enter event location")]
        public string Location { get; set; }

        [Required(ErrorMessage = "Please select bar location")]
        public double LocationLatitude { get; set; }

        [Required(ErrorMessage = "Please select bar location")]
        public double LocationLongitude { get; set; }

        public List<EventLanguageModel> Eventlanguagemodellist { get; set; }
    }

    public class EventLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public Language Language { get; set; }

        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter description name")]
        public string Description { get; set; }

        public virtual string LanguageName { get; set; }
    }

    public class EventViewModel : EventAddUpdateModel
    {

    }

    public class SingleEventViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageName { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTIme { get; set; }

        public string Location { get; set; }

        public double LocationLatitude { get; set; }

        public double LocationLongitude { get; set; }
    }

    public class ViewAllEventModel
    {
        public List<SingleEventViewModel> UpcomingEventList { get; set; }
        public List<SingleEventViewModel> PastEventList { get; set; }
    }
}
