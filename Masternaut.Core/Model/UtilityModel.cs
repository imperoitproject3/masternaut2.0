﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Model
{
    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }

    public class UploadImageModel
    {
        public string imageUrl { get; set; }
        public string saveFilePath { get; set; }
        public string hdnImageName { get; set; }
    }

    public class SaveImageModel
    {
        public string imgBase64String { get; set; }
        public string saveFilePath { get; set; }
        public string extension { get; set; }
        public string previousImageName { get; set; }
    }

    public class BreadcrumbModel
    {
        public string actionTitle { get; set; }
        public string actionName { get; set; }
        public string actioncontroller { get; set; }
        public string subActionTitle { get; set; }
    }

    public class DropdownModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class HomeModel
    {
        public List<SingleStoriesViewModel> StoriesList { get; set; }
    }

    public class ContactUsModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage ="Please enter name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Please enter fleet size")]
        public string FleetSize { get; set; }

        [Required(ErrorMessage ="Please enter email address")]
        [EmailAddress(ErrorMessage = "Please enter valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "You must provide a phone number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^([0-9]{9}|[0-9]{10}|[0-9]{11}|[0-9]{12}|[0-9]{13})$", ErrorMessage = "Invalid Mobile Number.")]
        public string Phone { get; set; }

        public string Message { get; set; }

        public ContactUsType Type { get; set; }
    }

    public class ContactUsViewModel : ContactUsModel
    {
        public DateTime DateTime { get; set; }
    }

    public class JobApplyModel
    {
        public long Id { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string JobappliedFor { get; set; }

        public string FileName { get; set; }
    }

    public class JobApplyViewModel : JobApplyModel
    {
        public DateTime DateTime { get; set; }
    }
}
