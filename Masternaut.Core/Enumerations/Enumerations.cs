﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Core.Enumerations
{
    public enum ResponseStatus
    {
        /// <summary>
        /// request failed due to some reason.
        /// </summary>
        Failed = 0,
        /// <summary>
        /// User need to provide email address.
        /// </summary>
        EmailRequired = 2,
        /// <summary>
        /// User need to provide phone number.
        /// </summary>
        VerifyPhone = 3,
        /// <summary>
        ///If facebook user not register
        /// </summary>
        UserNotExist = 4,
        /// <summary>
        /// system signout the app or website forcefully.
        /// </summary>
        ForceSignout = 100,
        /// <summary>
        /// request succeeded and that the requested information is in the response.
        /// </summary>
        Success = 1,
        /// <summary>
        /// that the request has been successfully processed and that the response is intentionally blank.
        /// </summary>
        NoContent = 204,
        /// <summary>
        /// that the requested resource requires authentication.
        /// </summary>
        Unauthorized = 401,
        /// <summary>
        /// that the client should switch to a different protocol such as TLS/1.0.
        /// </summary>
        UpgradeRequired = 426,
        /// <summary>
        /// indicates that a generic error has occurred on the server.
        /// </summary>
        InternalServerError = 500,
        /// <summary>
        /// that the server does not support the requested function.
        /// </summary>
        NotImplemented = 501,
        /// <summary>
        /// the requested resource does not exist on the server.
        /// </summary>
        NotFound = 400,
        /// <summary>
        ///  that the client did not send a request within the time the server was expecting the request.
        /// </summary>
        RequestTimeout = 408,
        /// <summary>
        /// that the server is temporarily unavailable, usually due to high load or maintenance.
        /// </summary>
        ServiceUnavailable = 503,

        AdminBlocked = 10,
        InternalError = 500,
        ActivationRequired = 307,
        SongsNotAvailable = 5

    }

    public enum Platform
    {
        Android,
        IOS,
        Web
    }

    public enum AlertStatus
    {
        Danger = 0,
        Success = 1,
        IUnfo = 2,
        Warning = 3,
        Primary = 4
    }

    public enum Language
    {
        English = 1,
        Dutch = 2,
        French = 3
    }

    public enum ContactUsType
    {
        None = 0,
        Inquiry = 1,
        Feedback = 2,
        Brochure = 3,
        Sales = 4,
        Support = 5,
        VehicleFleetTracking = 6
    }

    public enum UserRoles
    {
        Admin = 0,
        MobileUser = 1,
    }
}
