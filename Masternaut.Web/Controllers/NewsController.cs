﻿using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    public class NewsController : BaseController
    {
        private readonly NewsService _service;
        public NewsController()
        {
            _service = new NewsService();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Index(long catid = 0)
        {
            BindCategory();
            var objNews = new ViewAllNewsModel();
            int intPageIndex = 1, TotalNews = 0;
            objNews.NewsList = await _service.GetAllNews(new PaginationModel { PageIndex = intPageIndex, Id = catid });
            objNews.LatestNewsList = await _service.GetlatestNews();
            TotalNews = await _service.GetTotalNewsCount();
            objNews.PageIndex = intPageIndex;
            objNews.PageCount = Math.Ceiling((decimal)(TotalNews > 0 ? (TotalNews / GlobalConfig.PageSize) : 0));
            return View(objNews);
        }

        public async Task<ActionResult> LoadMoreNews(int pageindex)
        {
            ViewAllNewsModel objNews = new ViewAllNewsModel();
            int TotalNews = 0;
            objNews.PageIndex = pageindex;
            objNews.NewsList = await _service.GetAllNews(new PaginationModel { PageIndex = pageindex });
            TotalNews = await _service.GetTotalNewsCount();
            objNews.PageCount = Math.Ceiling((decimal)(TotalNews > 0 ? (TotalNews / GlobalConfig.PageSize) : 0));
            return PartialView("_ViewNews", objNews.NewsList);
        }

        [HttpGet]
        public async Task<ActionResult> ViewNews(long Id)
        {
            BindCategory();
            SingleNewsViewModel model = new SingleNewsViewModel();
            model = await _service.GetNewsDetailsForWeb(Id);
            return View(model);
        }
    }
}