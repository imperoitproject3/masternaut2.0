﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    [RoutePrefix("all-solutions")]
    public class SolutionController : BaseController
    {
        [Route("View")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("masternaut-connect")]
        public ActionResult MasternautConnect()
        {
            return View();
        }

        [Route("vehicle-fleet-tracking")]
        public ActionResult VehicleFleetTracking()
        {
            return View();
        }

        [Route("driving-behaviour")]
        public ActionResult DrivingBehaviour()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("DrivingBehaviourNL");
                case Language.French:
                    return View("DrivingBehaviourFR");
                default:
                    return View();
            }
        }

        [Route("telematics-devices")]
        public ActionResult TelematicsDevices()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("TelematicsDevicesNL");
                case Language.French:
                    return View("TelematicsDevicesFR");
                default:
                    return View();
            }
        }

        [Route("partner-offers")]
        public ActionResult PartnerOffers()
        {
            return View();
        }

        [Route("process-services")]
        public ActionResult ProfessioneleDiensten()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("ProfessioneleDienstenNL");
                case Language.French:
                    return View("ProfessioneleDienstenFR");
                default:
                    return View();
            }

        }

        [Route("obd-devices")]
        public ActionResult ObdDevice()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("ObdDevicesNL");
                case Language.French:
                    return View("ObdDeviceFR");
                default:
                    return View();
            }
        }

        [Route("ColdChainTracking")]
        public ActionResult ColdChainTracking()
        {
            return View();
        }

        [Route("systems-integration")]
        public ActionResult systemsIntegration()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("systemIntegrationNL");
                case Language.French:
                    return View("systemIntegrationFR");
                default:
                    return View();
            }
        }

        [Route("driver-mobile-app")]
        public ActionResult DriverMobileApp()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("DriverMobileAppNL");
                case Language.French:
                    return View("DriverMobileAppFR");
                default:
                    return View();
            }
        }

        [Route("paperless")]
        public ActionResult Paperless()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("PaperlessNL");
                case Language.French:
                    return View("PaperlessFR");
                default:
                    return View();
            }
        }
    }
}