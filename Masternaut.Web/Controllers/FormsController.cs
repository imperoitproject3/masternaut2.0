﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    public class FormsController : BaseController
    {
        public ActionResult ContactUs(ContactUsType Type = ContactUsType.Inquiry)
        {
            var currentla = Utilities.GetCurrentLanguage();
            ViewBag.Type = Type;
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("ContactUsNL");
                case Language.French:
                    return View("ContactUsFR");
                default:
                    return View();
            }
        }
        public ActionResult ContactWithUs(ContactUsType Type = ContactUsType.Inquiry)
        {
            ViewBag.Type = Type;
            return View();
        }
        public ActionResult ReasonForInquiry()
        {
            return View();
        }

        public ActionResult CompanyBrochure()
        {
            return View();
        }

       

    }
}