﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    public class AboutController : BaseController
    {
        public ActionResult About()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("AboutNL");
                case Language.French:
                    return View("AboutFR");
                default:
                    return View();
            }
        }

        public ActionResult Ourbrand()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("OurbrandNL");
                case Language.French:
                    return View("OurbrandFR");
                default:
                    return View();
            }
        }

        public ActionResult HowOurTechnology()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("HowOurTechnologyNL");
                case Language.French:
                    return View("HowOurTechnologyFR");
                default:
                    return View();
            }
        }
        public ActionResult Careers()
        {
            BindJobList();
            var currentla = Utilities.GetCurrentLanguage();
            return View();
        }

        public ActionResult WhyMasternaut()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("WhyMasternautNL");
                case Language.French:
                    return View("WhyMasternautFR");
                default:
                    return View();
            }
        }

        public ActionResult PrivacyPolicy()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("PrivacyPolicyNL");
                case Language.French:
                    return View("PrivacyPolicyFR");
                default:
                    return View();
            }
        }

        public ActionResult Disclaimer()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("DisclaimerNL");
                case Language.French:
                    return View("DisclaimerFR");
                default:
                    return View();
            }
        }

        public ActionResult WhatWeDo()
        {
            var currentla = Utilities.GetCurrentLanguage();
            switch (currentla)
            {
                case Language.English:
                    return View();
                case Language.Dutch:
                    return View("WhatWeDoNL");
                case Language.French:
                    return View("WhatWeDoFR");
                default:
                    return View();
            }
        }
    }
}