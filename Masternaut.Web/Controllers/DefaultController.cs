﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Masternaut.Core.Model;
using Masternaut.Core.Resources;
using Masternaut.Service;
using TweetSharp;

namespace Masternaut.Web.Controllers
{
    public class DefaultController : BaseController
    {
        private readonly StoriesService _service;

        public DefaultController()
        {
            _service = new StoriesService();
        }
        public async Task<ActionResult> Index()
        {
            HomeModel model = new HomeModel();
            model.StoriesList = await _service.GetTopStories();
            return View(model);
        }

    }
}