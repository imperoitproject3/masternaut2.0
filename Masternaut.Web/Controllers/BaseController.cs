﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TweetSharp;

namespace Masternaut.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;

            HttpCookie langCookie = Request.Cookies["culture"];
            if (langCookie != null)
            {
                lang = langCookie.Value;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage[0] : "";
                if (userLang != "")
                {
                    lang = userLang;
                }
                else
                {
                    lang = LanguageManager.GetDefaultLanguage();
                }
            }
            new LanguageManager().SetLanguage(lang);
            return base.BeginExecuteCore(callback, state);
        }

        public BaseController()
        {
            BindLanguage();
            BindTweet();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            OnInitBreadcrumb();
        }

        public void OnBindMessage(ResponseStatus status, string message)
        {
            OnBindMessage((status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger), message);
        }

        public void OnBindMessage(AlertStatus status, string message)
        {
            TempData["alertModel"] = new AlertModel(status, message);
        }

        public void OnInitBreadcrumb()
        {
            string actionName = ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = ControllerContext.RouteData.Values["controller"].ToString();

            if (controllerName != "Home")
            {
                if (actionName == "Index")
                {
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        subActionTitle = controllerName
                    };
                }
                else
                {
                    ViewBag.Action = actionName;
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        actioncontroller = controllerName,
                        actionName = "Index",
                        actionTitle = controllerName,
                        subActionTitle = actionName
                    };
                }
            }
        }

        public void BindCategory()
        {
            using (CategoryService service = new CategoryService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetCategoryDropdown().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["CategoryList"] = dropDownList;
            }
        }
        public void BindJobList()
        {
            var JobForlist = new List<SelectListItem>();

            JobForlist.Add(new SelectListItem { Text = "Corporate BDM (Hunter)", Value = "Corporate BDM (Hunter)" });
            JobForlist.Add(new SelectListItem { Text = "Corporate BDM (Farmer)", Value = "Corporate BDM (Farmer)" });
            JobForlist.Add(new SelectListItem { Text = "National BDM (Hunter)", Value = "National BDM (Hunter)" });
            JobForlist.Add(new SelectListItem { Text = "National BDM (Farmer)", Value = "National BDM (Farmer)" });
            JobForlist.Add(new SelectListItem { Text = "Corporate Contract Manager", Value = "Corporate Contract Manager" });
            JobForlist.Add(new SelectListItem { Text = "National Contract Manager", Value = "National Contract Manager" });
            JobForlist.Add(new SelectListItem { Text = "Inside Contract Manager", Value = "Inside Contract Manager" });
            JobForlist.Add(new SelectListItem { Text = "Customer Success Analyst", Value = "Customer Success Analyst" });
            JobForlist.Add(new SelectListItem { Text = "IT Graduate", Value = "IT Graduate" });
            JobForlist.Add(new SelectListItem { Text = "Platform Architect", Value = "Platform Architect" });
            JobForlist.Add(new SelectListItem { Text = "Telesales", Value = "Telesales" });
            JobForlist.Add(new SelectListItem { Text = "Sales & Marketing Data Assistant", Value = "Sales & Marketing Data Assistant" });
            JobForlist.Add(new SelectListItem { Text = "Marketing Analyst", Value = "Marketing Analyst" });
            JobForlist.Add(new SelectListItem { Text = "Online & Social Media Manager", Value = "Online & Social Media Manager" });

            ViewData["JobForlist"] = JobForlist;
        }

        public void BindIndustryType()
        {
            using (IndustryTypeService service = new IndustryTypeService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetIndustryDropdown().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["IndustryTypeSList"] = dropDownList;
            }
        }

        public void BindLanguage()
        {
            var Languagelist = new List<SelectListItem>();
            Languagelist.Add(new SelectListItem { Text = Language.English.ToString(), Value = "en-GB" });
            Languagelist.Add(new SelectListItem { Text = Language.Dutch.ToString(), Value = "nl-NL" });
            Languagelist.Add(new SelectListItem { Text = Language.French.ToString(), Value = "fr-FR" });
            ViewData["LanguageList"] = Languagelist;
        }

        public void BindTweet()
        {
            try
            {
                var service = new TwitterService("NLF7yxkzxezmoZ04OFfI7FBbq", "pM0ZI8tp84C3qxGeOr3RD7Mq09OBMlpiKCUsPs0p0ROOVgeCK6");
                //AuthenticateWith(“Access Token”, “AccessTokenSecret”);
                service.AuthenticateWith("3088560927-aYoC7Jkd9ds49Z8gXgubwdCQzxoGZK9Zx3rEzsT", "1dEUrOPya7kpb59ZQlJh06PHvgpUD1pDavm3N6UQwW4Jz");
                //ScreenName=”screeen name not username”, Count=Number of Tweets / http://www.Twitter.com/masternaut.
                IEnumerable<TwitterStatus> tweets = service.ListTweetsOnUserTimeline(new ListTweetsOnUserTimelineOptions { ScreenName = "masternaut", Count = 5 });
                ViewBag.Tweets = tweets;
            }
            catch (Exception ex)
            {
            }
        }
    }
}