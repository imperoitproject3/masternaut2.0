﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Core.Resources;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    public class UtilityController : BaseController
    {
        private readonly ContactUsService _service;
        public UtilityController()
        {
            _service = new ContactUsService();
        }

        public ActionResult ChangeLanguage(string lang, string redirecturl)
        {
            new LanguageManager().SetLanguage(lang);
            return Redirect(redirecturl);
        }

        public ActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> SaveContactUs(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                mResult = await _service.SaveContactUs(model);
                if (model.Type == ContactUsType.Brochure)
                {
                    var currentlan = Utilities.GetCurrentLanguage();
                    var filename = string.Empty;

                    switch (currentlan)
                    {
                        case Language.English:
                            filename = "Masternaut_en.pdf";
                            break;
                        case Language.Dutch:
                            filename = "Masternaut_nl.pdf";
                            break;
                        case Language.French:
                            filename = "Masternaut_fr.pdf";
                            break;
                        default:
                            filename = "Masternaut_en.pdf";
                            break;
                    }

                    mResult.Result = GlobalConfig.BrochurefileBaseUrl + filename;
                    mResult.Message = ResponseMessages.BrochureDownload;
                }
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mResult.Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> SaveJobApply(JobApplyModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file != null && file.ContentLength > 0)
                    {
                        //get filename
                        string ogFileName = file.FileName.Trim('\"');
                        string shortGuid = ShortGuid.NewGuid().ToString();
                        var thisFileName = "_" + shortGuid + Path.GetExtension(ogFileName);
                        file.SaveAs(Path.Combine(GlobalConfig.ResumePath, thisFileName));
                        model.FileName = thisFileName;
                    }

                }
                mResult = await _service.SaveJobApply(model);
                OnBindMessage(mResult.Status, mResult.Message);
                return RedirectToAction("careers", "about");
            }
            else
            {
                mResult.Message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault();
                OnBindMessage(mResult.Status, mResult.Message);
                return RedirectToAction("careers", "about", model);
            }
        }
    }
}