﻿using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    public class EventController : BaseController
    {
        private readonly EventService _service;
        public EventController()
        {
            _service = new EventService();
        }

        public async Task<ActionResult> Index()
        {
            var objEvent = await _service.GetEventList();
            return View(objEvent);
        }

        public async Task<ActionResult> ViewEvent(long Id)
        {
            var eventdetails = await _service.GetEventDetailsForWeb(Id);
            return View(eventdetails);
        }
    }
}