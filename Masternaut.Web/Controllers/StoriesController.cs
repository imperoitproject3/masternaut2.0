﻿using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
    public class StoriesController : BaseController
    {
        private StoriesService _service;

        public StoriesController()
        {
            _service = new StoriesService();
        }

        public async Task<ActionResult> Index()
        {
            return View(await _service.GetAllStories());
        }

        public async Task<ActionResult> ViewStories(long Id)
        {
            SingleStoriesViewModel objStories = await _service.GetStoriesDetails(Id);
            return View(objStories);
        }
        public ActionResult Customer()
        {
            return View();
        }
    }
}