﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Web.Controllers
{
   [RoutePrefix("benefits-of-telematics")]
    public class BenefitsOfTelematicsController : BaseController
    {
        [Route("view")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("be-safer")]
        public ActionResult BeSafer()
        {
            return View();
        }
        [Route("save-more")]
        public ActionResult SaveMore()
        {
            return View();
        }
        [Route("boost-service")]
        public ActionResult BoostService()
        {
            return View();
        }
        [Route("go-greener")]
        public ActionResult GoGreener()
        {
            return View();
        }
    }
}