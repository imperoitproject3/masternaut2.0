﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Masternaut.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsSecureConnection && !Request.IsLocal)
            {
                if (Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTP://WWW"))
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
                else
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://www."));
            }
            else if (!Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTPS://WWW") && !Request.IsLocal)
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("https://", "https://www."));
            }
        }
    }
}
