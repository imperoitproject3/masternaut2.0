﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Masternaut.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();


            routes.MapRoute(
            name: "Disclaimer",
            url: "about/disclaimer/",
            defaults: new { controller = "about", action = "Disclaimer" });

            routes.MapRoute(
            name: "PrivacyPolicy",
            url: "about/privacy-policy/",
            defaults: new { controller = "about", action = "PrivacyPolicy" });

            routes.MapRoute(
         name: "WhyMasternaut",
         url: "about/why-masternaut/",
         defaults: new { controller = "about", action = "WhyMasternaut" });

            routes.MapRoute(
         name: "AboutUsHow",
         url: "about/how-our-technology",
         defaults: new { controller = "about", action = "HowOurTechnology" });

            routes.MapRoute(
          name: "AboutUs",
          url: "about/",
          defaults: new { controller = "about", action = "about" });

            routes.MapRoute(
          name: "OurCustomer",
          url: "our-customer/",
          defaults: new { controller = "Stories", action = "Customer" });

            routes.MapRoute(
           name: "CompanyBrochure",
           url: "company-brochure/",
           defaults: new { controller = "Forms", action = "CompanyBrochure" });

            routes.MapRoute(
            name: "ViewStories",
            url: "customer-case-studies/{title}/{id}",
            defaults: new { controller = "Stories", action = "ViewStories", id = typeof(long) });

            routes.MapRoute(
            name: "ViewEvent",
            url: "event/{title}/{id}",
            defaults: new { controller = "Event", action = "ViewEvent", id = typeof(long) });

            routes.MapRoute(
             name: "NewsByCategory",
             url: "news-category/{category}/{id}",
             defaults: new { controller = "News", action = "Index", id = typeof(long) });

            routes.MapRoute(
            name: "ViewNews",
            url: "news-detail/{title}/{id}",
            defaults: new { controller = "News", action = "ViewNews", id = typeof(long) });

            routes.MapRoute(
            name: "CaseStudies",
            url: "customer-case-studies/",
            defaults: new { controller = "Stories", action = "Index" });

            routes.MapRoute(
           name: "ReasonForInquiry",
           url: "connect-us/reason-for-enquiry/",
           defaults: new { controller = "Forms", action = "ReasonForInquiry", });

            routes.MapRoute(
            name: "ContactUs",
            url: "Connect-us/{Type}",
            defaults: new { controller = "Forms", action = "ContactUs", Type = UrlParameter.Optional });

            routes.MapRoute(
             name: "ContactWithUs",
             url: "Connect-with-us/{Type}",
             defaults: new { controller = "Forms", action = "ContactUs", Type = UrlParameter.Optional });

            routes.MapRoute(
              name: "News",
              url: "news/",
              defaults: new { controller = "News", action = "Index" });

            routes.MapRoute(
            name: "Event",
            url: "event/",
            defaults: new { controller = "Event", action = "Index" });

            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional });
        }
    }
}
