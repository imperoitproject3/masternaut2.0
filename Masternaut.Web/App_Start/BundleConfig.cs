﻿using System.Web.Optimization;

namespace Masternaut.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jQuery").Include(
                        "~/Content/js/jquery-ui.min.js",
                        "~/Content/js/jquery.jcarousel.min.js",
                        "~/Content/js/modernizr.custom.js",
                        "~/Content/js/menu/jquery.mmenu.all.min.js",
                        "~/Content/js/menu/jquery.mmenu.dropdown.min.js",
                        "~/Content/js/slick/slick.min.js",
                        "~/Content/js/magnific-popup/jquery.magnific-popup.min.js",
                        "~/Content/js/main30f4.js",
                        "~/Content/js/xys1yjs.js",
                        "~/Content/js/jquery.validate.js",
                        "~/Content/js/jquery.validate.unobtrusive.min.js",
                        "~/Content/js/jquery.unobtrusive-ajax.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/ContentJs").Include(
                        "~/Content/js/jquery.easing.1.3.js",
                        "~/Content/js/jquery.themepunch.revolution.min.js",
                        "~/Content/js/jquery.themepunch.tools.min.js",
                        "~/Content/js/skrollr.js",
                        "~/Content/js/jquery.waypoints.min.js",
                        "~/Content/js/jquery.scrollmagic.min.js",
                        "~/Content/js/greensock/TweenMax.min.js",
                        "~/Content/js/slick.min.js",
                        "~/Content/js/jquery.quicksand.js",
                        "~/Content/Plugins/Toaster/toastr.min.js"
                        ));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/main.css",
                      "~/Content/css/slick.css",
                      "~/Content/css/magnific-popup.css",
                      "~/Content/css/menu.css",
                      "~/Content/css/atc/atc-base.css",
                      "~/Content/css/settings.css",
                      "~/Content/Plugins/Toaster/toastr.css",
                      "~/Content/css/adaptiveForm.css",
                      "~/Content/css/form.css"
                        ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
