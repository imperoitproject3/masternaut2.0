﻿function displayOverlay() {
    if ($("body").find("#overlay").length <= 0) {
        $("<table id='overlay'><tbody><tr><td></td></tr></tbody></table>").appendTo("body");
        $("#loader-post").show();
    }
}
function removeOverlay() {
    $("#overlay").remove();
    $("#loader-post").hide();
}
function initializejsforflip() {
    $(".profile-bx .pf-chet").click(function () {
        $(this).parents('.profile-bx').addClass('active');
    });
    $(".profile-bx .close-profile-message").click(function () {
        $(this).parents('.profile-bx').removeClass('active');
    });
}