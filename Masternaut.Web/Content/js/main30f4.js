//Cookie functions ---------------------------------------/
function createCookie(name,value,days){
	if (days){
		var date = new Date();
		date.setTime( date.getTime() + (days*24*60*60*1000) );
		var expires = '; expires=' + date.toGMTString();
	}
	else var expires = '';
	document.cookie = name + '=' + value + expires + '; path=/';
}
function readCookie(name){
	var nameEQ = name + '=';
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++){
		var c = ca[i];
		while (c.charAt(0 )== ' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function eraseCookie(name){
	createCookie(name,'',-1);
}

//Other functions ---------------------------------------/

function placeholders() {

    $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
        }
    }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
        }
    }).blur();
    $('[placeholder]').parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        })
    });
}

function equalHeights(group) {
    var tallest = 0;
    group.css('height','').each(function() {
        var thisHeight = $(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.height(tallest).last().addClass('last');
}   
function equalHeightsRows(group, numCols){
    
    var totalItems = group.length;
    var numRows;
    
    numRows = Math.ceil( totalItems / numCols );
        
    group.each(function(i){
        var p = Math.ceil( (i + 1) / numCols );
        $(this).addClass( 'row-' + p.toString() );
    });
    
    for( var p = 1; p <= numRows; p++){
        equalHeights( group.filter( '.row-' + p.toString() ) );
    }
}

function setupHeights() {

    equalHeights($('.page-pods .pod .pod-heading'));
    equalHeights($('.content-pods .pod .pod-inner .pod-heading'));
    equalHeights($('.main-wrap .content-pods .pod .pod-text h4'));
	equalHeights($('.content-pods .pod .cat-title'));
	equalHeights($('.page-pods .pod .inner-text'));
    equalHeights($('.content-pods .pod .inner-text'));
    equalHeights($('.home-circles .content-pods .pod .inner-text'));
    equalHeights($('.case-studies .content-pods .pod .inner-text'));
	equalHeights($('.double-content-block.blue .content-left, .double-content-block.blue .content-right'));
	equalHeights($('.latest-post .post-content, .latest-post .post-image'));
	equalHeights($('.featured-article .post-content, .featured-article .post-image'));
	equalHeights($('.next-event .event-content, .next-event .event-img'));
	equalHeights($('.events-list .event-title'));
	equalHeights($('.events-list .event-text'));
	equalHeights($('.roi-calculator .span6'));
}


// Setup site ------------------------------------------/
function setupMap(mapActive,latitude,longitude,zoom,marker,offsetx,offsety) {
	var map;

	function initialize() {
	    if(mapActive){
	        var latlng;
	        latlng = new google.maps.LatLng(latitude, longitude);
	        var myOptions = {
	            zoom: zoom,
	            center: latlng,
	            scrollwheel: false,
	            mapTypeId: google.maps.MapTypeId.ROADMAP,
	            disableDefaultUI: true,
	            draggable: false,
	            zoomControl: false,
	            scrollwheel: false,
	            disableDoubleClickZoom: true
	        };
	        map = new google.maps.Map(document.getElementById("map"), myOptions);

	        google.maps.event.addDomListener(window, "resize", function() {
			   var center = map.getCenter();
			   google.maps.event.trigger(map, "resize");
			   map.setCenter(center); 
			});

			if(offsetx | offsety) {
				map.panBy(offsetx,offsety);
			}
	        if(marker != false) {
	        	addMarker(latlng);
	        }
	    }   
	}

	function addMarker(location) {
	   marker = new google.maps.Marker({
	            position: location,
	            draggable: false,
	            map: map
	        });     
	}

	window.onload = function () {
	    initialize();
	}
}
function setup404() {
	var markerInner = $('main.error404 .marker-inner').outerHeight();
	$('main.error404 .marker').height(markerInner);
}

//Header functions ---------------------------------------/
function setupHead() {

	var distanceY = document.documentElement.scrollTop,
		header = $('header'),
		shrinkOn = 200;

	if(window.addEventListener) {
		window.addEventListener('scroll', function(e) {
			distanceY = window.pageYOffset || document.documentElement.scrollTop;
			if(distanceY > shrinkOn) {
				header.addClass("smaller");
			} else {
				if(header.is(".smaller")) {
					header.removeClass("smaller");
				}
			}
		});
	} else {
		window.attachEvent('scroll', function(e) {
			distanceY = window.pageYOffset || document.documentElement.scrollTop;
			if(distanceY > shrinkOn) {
				header.addClass("smaller");
			} else {
				if(header.is(".smaller")) {
					header.removeClass("smaller");
				}
			}
		});
	}

        header.find("li.dead > a").click(function (e) {
                e.preventDefault();
        });

	header.on("mouseenter", function() {
		$(this).removeClass("smaller");
	})
	.on("mouseleave", function(){
		if(distanceY > shrinkOn) {
			header.addClass("smaller");
		}
	});
}
function setupTopNav() {
	$('.main-nav .top-level.has-sub').each(function() {
		$(this).mouseover(function() {
			$('.main-nav .sub-nav').hide();
			$(this).find('.sub-nav').show();
			$(this).addClass('active');
		})
		.mouseout(function() {
			$('.main-nav .sub-nav').hide();
			$(this).removeClass('active');
		});
	});
    /*
	$('.mobile-nav .open-sub').click(function(e) {
		var parent = $(this).parent('li');
		var sub = $(this).siblings('ul.sub-nav');

		$('.mobile-nav li.top-level').removeClass('open');
		$('.mobile-nav .sub-nav').slideUp('fast');

		if(sub.is(':hidden')) {
			sub.slideToggle();
			parent.toggleClass('open');
		}

		return false;
	});

	$('.mobile-nav-btn').click(function(e) {
		var nav = $('.mobile-nav > ul');

		if(nav.is(':hidden')) {
			nav.stop().show().animate({
				left: 0
			});
			$(this).addClass('open');
			$('body').css('overflow', 'hidden');
		} else {
			nav.stop().animate({
				left: '100%'
			}, 500, function(){
				nav.hide();
			});
			$(this).removeClass('open');
			$('body').css('overflow-y', 'scroll');
		}
		
		return false;
	});

	$(window).resize(function(e) {
		var nav = $('.mobile-nav > ul');

		if(nav.is(':visible')) {
			nav.stop().animate({
				left: '100%'
			}, 500, function(){
				nav.hide();
			});
			$('.mobile-nav-btn').removeClass('open');
			$('body').css('overflow-y', 'scroll');
		}
	});*/
}

//Footer functions ---------------------------------------/
function setupFooter() {
	var tweetContainer = $('footer .tweets');
	var tweets = tweetContainer.find('.tweet');

	if(tweets.length > 1) {
		tweetContainer.slick({
			arrows: true,
			adaptiveHeight: true,
			infinite: true,
			prevArrow: $('.tweet-nav .left a'),
			nextArrow: $('.tweet-nav .right a')
		});
	}
}
function setupCookieStatement() {
	var cookieStatement = $('.cookie-statement');
	var cookieContinue = $('.cookie-statement .btn-primary');

	if(!readCookie('cookiesContinue')) {
		setTimeout(function() {
			cookieStatement.slideDown();
		},2000);
		cookieContinue.click(function(e){
			cookieStatement.slideUp();
			createCookie('cookiesContinue',1);
			return false;
		});
	}
}

//Page functions ---------------------------------------/
function sliderNav(cssClass) {
	var nav = '<ul class="slider-nav ' + cssClass + '">';
		nav += '<li class="left"><a href="">&lt;</a></li>';
		nav += '<li class="right"><a href="">&gt;</a></li>';
		nav += '</ul>';

	return nav;
}
function setupSliders() {

	var containers = $('.home-modules .content-pods, .other-products.blue-block .content-pods'),
		max_pods = $('body').hasClass('home') ? 4 : 3;

	if(containers.find('.pod').length > max_pods) {
	    containers
	    	.after(sliderNav('module-nav'))
		    .slick({
				arrows: true,
				infinite: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: $('.module-nav .left a'),
				nextArrow: $('.module-nav .right a'),
				responsive: [
					{
						breakpoint: 860,
						settings: {
							slidesToShow: 2
						}
					},
					{
						breakpoint: 650,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});
	}

	var quotes = $('.quotes');
	if(quotes.find('.quote').length > 1) {
		quotes.slick({
			arrows: false,
			infinite: true,
			dots: true
		});
	}

	$('.inner-banners.slider > div')
		.after(sliderNav('inner-slider-nav'))
		.slick({
			arrows: true,
			infinite: true,
			prevArrow: $('.inner-slider-nav .left a'),
			nextArrow: $('.inner-slider-nav .right a')
		});
}

function setupPageNav() {

	var page = $('body.products');
	var waypoints = page.find('*[data-waypoint]');
	var pageNav = page.find('.page-nav ul');
	
	waypoints.each(function(i) {
		var waypoint = $(this).attr('data-waypoint');
		var heading = $(this).find('h1,h2,h3,h4,span').html();

		var html = "";
		html += '<li>';
		if(i == 0) {
			html += '<a href="" data-slide="' + waypoint + '" class="slide-btn active">';
		} else {
			html += '<a href="" data-slide="' + waypoint + '" class="slide-btn">';
		}
		html += '<span>' + heading + '</span>';
		html += '</a>';
		html += '</li>';

		pageNav.append(html);
	});

	$('.page-nav').css('margin-top','-'+ (pageNav.outerHeight() / 2) +'px');

}
function setupBanners() {
	$('.banner-layers').show().revolution({
		dottedOverlay:"none",
		hideTimerBar:"on",
		delay: 9000,
		startwidth: 1140,
		startheight: 450,
		fullwidth:"on",
		fullScreen: "off",
		autoHeight: "off",
		forceFullWidth:"off"
	});
}
function setupParallax() {
    $('.parallax').each(function() {
        var bgPosition          = $(this).css('backgroundPosition');
        var bgY                 = bgPosition.split(' ')[1];
        var bgPositionEnd       = '';
        if(bgY === '50%') {
            bgPositionEnd = '50% 70%';
        } else {
            bgPositionEnd = '50% 50%';
        }

        $(this)
            .attr('data-center', 'background-position: ' + bgPosition)
            .attr('data-top-bottom', 'background-position: ' + bgPositionEnd)
            .attr('data-anchor-target', '.'+ $(this).parent().attr('class').split(' ')[1]);
    });
}
function setupWaypoints() {

	slides = $('div[data-waypoint],section[data-waypoint]');
	htmlbody = $('html,body');

	// Waypoints down
	slides.waypoint(function(direction) {
		//cache the variable of the data-slide attribute associated with each slide
        dataslide = $(this).attr('data-waypoint');
		
		if(direction === 'down') {
	        $('.page-nav .slide-btn').removeClass('active');
	        $('.page-nav .slide-btn[data-slide='+dataslide+']').addClass('active');
	    }
    }, { offset: '50%' });

	// Waypoints up
    slides.waypoint(function(direction) {
		//cache the variable of the data-slide attribute associated with each slide
        dataslide = $(this).attr('data-waypoint');
		
		if(direction === 'up') {
	        $('.page-nav .slide-btn').removeClass('active');
	        $('.page-nav .slide-btn[data-slide='+dataslide+']').addClass('active');
	    }
    }, { 
    	offset: function() {
    		return $.waypoints('viewportHeight') / 2 - $(this).outerHeight();
    	}
    });

	function goToByScroll(dataslide) {
		var scroll = $('div[data-waypoint="' + dataslide + '"],section[data-waypoint="' + dataslide + '"]').offset().top - 100;
		if(dataslide == 1) {
			var scroll = $('div[data-waypoint="' + dataslide + '"],section[data-waypoint="' + dataslide + '"]').offset().top - 140;
		}
        htmlbody.animate({
            scrollTop: scroll
        }, 2000, 'easeInOutQuint');
    }

    $('a.slide-btn').click(function(e) {
    	var waypoint = $(this).attr('data-slide');
    	goToByScroll(waypoint);
    	return false;
    });

    //Stopping scroll animation when user scrolls
    htmlbody.bind('scroll mousedown DOMMouseScroll mousewheel keyup', function() {
    	$(this).stop();
    });

    setupParallax();

    //refresh stellar on window resize
    $(window).resize(function() {
        setupParallax();
    });

    // Animating objects
    $('.animate').each(function() {

    	var img = $(this);

    	// init controller
		var controller = new ScrollMagic();

		var animateFrom = img.attr('data-from');
		var limit = img.attr('data-limit');
	    var speed = img.attr('data-speed');

		if(animateFrom == "top") {
	    	var tween = TweenMax.to(img,speed, {top: limit});
		} else if(animateFrom == "bottom") {
	    	var tween = TweenMax.to(img,speed, {bottom: limit});
		} else if(animateFrom == "left") {
	    	var tween = TweenMax.to(img,speed, {left: limit});
		} else if(animateFrom == "right") {
	    	var tween = TweenMax.to(img,speed, {right: limit});
		}

    	// build scene
		var scene = new ScrollScene({triggerElement: img.parent('div')})
			.setTween(tween)
			.addTo(controller);

		img.parent('.block-img').css('min-height',img.outerHeight());
    });

    /*if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
        skrollr.init({
            forceHeight: false
        });
    }*/

}


// Pages -----------------------------------------------/
function setupConsultancyPage() {


	$('.people-slider').each(function() {

		if($(this).find('.team-member').length > 3) {
			isInfinate = true;
			hasArrows = true;
		} else {
			isInfinate = false;
			hasArrows = false;
		}

		$(this).slick({
			arrows: hasArrows,
			infinite: isInfinate,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 860,
					settings: {
						arrows: true,
						slidesToShow: 3,
						infinite: true
					}
				},
				{
					breakpoint: 650,
					settings: {
						arrows: true,
						slidesToShow: 2,
						infinite: true
					}
				},
				{
					breakpoint: 460,
					settings: {
						arrows: true,
						slidesToShow: 1,
						infinite: true
					}
				}
			]
		});
	});

	$('.consultancypage .teams a').click(function(e) {

		var team = $(this).data('team');
		var currentTeam = $('.consultancypage .people-wrap[data-team='+team+']');
		var slider = currentTeam.find('.people-slider');
		var prev = currentTeam.find('.left a');
		var next = currentTeam.find('.right a');

		$('.people-wrap').removeClass('visible').height(0);
		currentTeam.addClass('visible').height(slider.outerHeight());

		$('.consultancypage .teams a').removeClass('active');
		$(this).addClass('active');			

		return false;
	});
}

function setupBlogPages() {

	$('.toggle-open').click(function(e) {
		var parent = $(this).parent('li');
		$(this).siblings('ul').stop().slideToggle('fast');
		parent.toggleClass('open');
	});
}

function setupTeamPage() {

	$('.team a.more').each(function() {
		var teamDiv = $(this).parent('.team');
		var description = $(this).siblings('.description');
		var position = $(this).position();

		$(this).click(function(e) {
			$('.description').not(description).animate({
				top: ['-100%','swing'],
				left: ['100%','swing'],
				opacity: ['0','swing']
			}, {duration: 300, easing: "easein" });
			description.animate({
				top: [position.top + 'px','swing'],
				left: ['0','swing'],
				opacity: ['1','swing']
			}, { duration: "normal", easing: "easein" });
			return false;
		});
	});

	$('.description .close').click(function(e) {
		$(this).parent('.description').animate({
			top: ['-100%','swing'],
			left: ['100%','swing'],
			opacity: ['0','swing']
		}, {duration: 300, easing: "easein" });
		return false;
	});
}

function setupVacancies() {

	var vacancies = $('.vacancies');
	var form = vacancies.find('div.apply');
	var dropdownList = vacancies.find('.dropdown > div');

	// Setting up job accordion
	vacancies.find('.toggle-open').click(function(e) {
		var jobHead = $(this).parent('.job-head');
		var jobContent = jobHead.siblings('.job-content');

		jobHead.find('.toggle-open').toggleClass('close');

		jobContent.stop().slideToggle('normal');

		jobHead.toggleClass('open');
		$(this).toggleClass('open');

		return false;
	});

	// Selecting job and scrolling to form
	vacancies.find('.btns a.apply').click(function(e) {
		var option = $(this).data('job');
		dropdownList.find('p.selected').html(option);
		dropdownList.find('select option').each(function() {
			if($(this).val() == option) {
				$(this).attr('selected',true);
			}
		});
		$('html,body').animate({
            scrollTop: form.offset().top - 100
        }, 500, 'easeInOutQuad');
		return false;
	});
}

function setupCaseStudies() {

	$('.case-study-banner .quote-text p').last().addClass('last');

	// First collection of case studies for filtering
    var caseStudies = $('#case-study-posts');
    // Second collection of case studies for filtering
    var data = caseStudies.clone();

	$('.case-studies .dropdown').each(function() {
		var dropdown = $(this);

		// On selection
		dropdown.find('.dropdown-list a').click(function(e) {

			// Selecting option based on new list click
			var option = $(this).find('.value').html();
			var optionFormated = $(this).parent('li').data('type');
			dropdown.find('p.selected').attr('data-type',optionFormated).html(option);
			dropdown.find('.dropdown-list').stop().slideUp('fast');
			dropdown.find('.dropdown-arrow').removeClass('closed');

			// Setting classes for filter
			var classString = "";

			if(optionFormated != 'all') {
				var classString = "." + optionFormated;
			}

			var parent = dropdown.parent('div.filter');

			// Finding all other filters and adding selected values
			parent.siblings('.filter').each(function() {
				var selected = $(this).find('p.selected');

				if(selected.html() != "" && selected.html() != "All") {
					var type = $(this).find('p.selected').attr('data-type');
					classString += "."+type;
				}
			});

			var filteredData = data.find('li'+classString);

			// Running quicksand for filtering
			caseStudies.quicksand(filteredData, {
	            duration: 500,
	            easing: 'easeInOutQuad',
	            atomic: 'true',
	            adjustWidth: 'dynamic',
	            adjustHeight: true
	        });

			// Applying numbers to filter options
	        var industryType = $('.customers .filter.industry-type');
	        $('.case-studies .filter').find('.option').each(function() {
	        	var filter = $(this).parents('.filter');

	        	// Current option's type, value and initial count
	        	var type = $(this).attr('data-type');
	        	var value = $(this).find('a .value').html()
	        	var contents = $(this).find('a .count').html();

	        	// Other selected filters
	        	var industrySelected = industryType.find('p.selected');

	        	// Assigning class string
	        	var classString = '.' + type;
	        	var selectedType = '';

	        	// Setting classes based on current filter and other filter selected values
	        	if(filter.is('.industry-type') && companySelected.html() != "" && companySelected.html() != "All") {
	        		selectedType = '.'+companySelected.attr('data-type');
	        		classString += '.'+companySelected.attr('data-type');
	        	}

	        	if(value == "All") {
	        		var newCount = data.find('li'+selectedType).length;
	        		$(this).find('a .count').html('(' + newCount + ')');
	        	} else {
	        		var newCount = data.find('li'+classString).length;
	        		$(this).find('a .count').html('(' + newCount + ')');
	        		if(newCount == 0) {
	        			$(this).find('a').addClass('disabled');
	        		} else {
	        			$(this).find('a').removeClass('disabled');
	        		}
	        	}
	        });

		});
	});

	$('.secondary-content.accordion .accordion-content').each(function() {

		var heading = $(this).find('h4');
		var content = $(this).find('.body-text');

        heading.click(function(e) {
        	heading.toggleClass('active');
        	content.stop().slideToggle(function() {
        	
        	});
        	return false;
        });
	});
}

function setupEvents() {

	$('.event-categories .option a').click(function() {
		var category = $(this).parent('li').attr('data-type');
		var collection = $('.upcoming-events[data-collection='+category+']');

		$('.upcoming-events').slideUp();

		collection
			.slick('unslick')
			.slideDown()
			.slick({
				arrows: true,
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				responsive: [
					{
						infinite: true,
						breakpoint: 860,
						settings: {
							slidesToShow: 2
						}
					},
					{
						infinite: true,
						breakpoint: 650,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});
	});

	$('.upcoming-events')
		.slick({
			arrows: true,
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					infinite: true,
					breakpoint: 860,
					settings: {
						slidesToShow: 2
					}
				},
				{
					infinite: true,
					breakpoint: 650,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});

	$('.past-events')
		.slick({
			arrows: true,
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 860,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
}

function setupROI() {

	// Setting up dropdown list
	$('.roi-form').find('.dropdown').each(function() {
		var label = $(this).find('label');
		$(this).find('p.selected').html(label.html());
	});

	// Setting up radiobuttons
	$('.roi-form .radiobuttonlist').each(function() {
		var list = $(this);

		$(this).find('label').click(function(e) {
			list.find('label').removeClass('active');
			$(this).addClass('active');
		});

		$(this).find('input:checked').each(function() {
			$(this).parents('.option').find('label').addClass('active');
		});
	});

	// Setting up fleet count number spinners
	$('.roi-form').find('.hgvfleetcount input, .vanfleetcount input, .pickupfleetcount input, .carfleetcount input')
		.spinner({
			min: 0,
			icons: {
				down: "ui-icon-carat-1-n",
				up: "ui-icon-carat-1-s"
			}
		});

	// Increasing spinner number on label click
	$('.roi-form').find('.hgvfleetcount label, .vanfleetcount label, .pickupfleetcount label, .carfleetcount label')
		.click(function() {
			var input = $(this).next('div').find('input');
			var val = parseInt(input.val()) + 1;
			input.val(val);
		});

	// Preventing non-numerical keys on keypress and preventing cut, copy and paste
	$('.roi-form').find('.hgvfleetcount input, .vanfleetcount input, .pickupfleetcount input, .carfleetcount input')
		.keypress(function(e) {

			var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
	        if (!($.inArray(e.which, key_codes) >= 0)) {
	           return false;
	        }
		})
		.bind("cut copy paste", function(e) {
			return false;
		});

	// Removing last word in mileage labels
	var fields = $('.totalannualmileagehgv, .totalannualmileagevan, .totalannualmileagepickup, .totalannualmileagecar');
	var labels = fields.find('label');

	labels.each(function() {
		var label = $(this);
		var labelText = label.html();
		labelText = labelText.trim(labelText);
		var lastIndex = labelText.lastIndexOf(" ");
		var newLabel = labelText.substring(0,lastIndex);
		label.html(newLabel);
	});

	// Formatting numbers on keyup
	fields.find('input').keyup(function(e) {

		// skip for arrow keys
        if(e.which >= 37 && event.which <= 40){
            return false;
        }
        var $this = $(this);
        var num = $this.val().replace(/,/gi, "")
            .split("").reverse().join("");

        var num2 = RemoveRougeChar(
            num.replace(/(.{3})/g,"$1,").split("")
                .reverse().join("")
        );

        // the following line has been simplified.
        // Revision history contains original.
     
        $this.val(num2);
	});

	// Preventing cut, copy and paste
	fields.bind("cut copy paste", function(e) {
		return false;
	});

	// Preventing non-numerical keys on keypress
	fields.find('input').keypress(function(e) {

		var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8];
        if (!($.inArray(e.which, key_codes) >= 0)) {
           return false;
        }
	});

	function RemoveRougeChar(convertString){
	    if(convertString.substring(0,1) == ","){
	        return convertString.substring(1, convertString.length)            
	    }
	    return convertString;
	}

	// Savings value
	$('.roi-calculator-step-2 .savings input').attr('readonly','readonly');

	// Setting placeholders on step 2 fields
	$('.roi-calculator-step-2 .span6 .textfield').each(function() {
		var label = $(this).find('label');
		var input = $(this).find('input');

		// Retrieving value without inner html (span)
		var value = label.clone().children().remove().end().text();

		label.hide();
		input.attr('placeholder',$.trim(value));
	});

	var journeyRadius = $('.selectjourneyradius');
	var journeyRadios
	$('.areyourjourneyslocalornational .option').each(function() {
		
		if($(this).hasClass('local')) {
			if($(this).find('input').is(':checked')) {
				journeyRadius.show();
			}
			$(this).find('label').click(function() {
				journeyRadius.show();
			});
		} else {
			$(this).find('label').click(function() {
				journeyRadius.hide();
			});
		}
	});

}

function setupContact() {

	// Setting up placeholders
/*
	$('.contactpage .span6 .textfield').each(function() {
		var label = $(this).find('label');
		var input = $(this).find('input');
		var requiredVal = input.attr('data-var-required');

		// Retrieving value without inner html (span)
		var value = label.clone().children().remove().end().text();
		value = $.trim(value);

		if(requiredVal != "") {
			value += " *";
		}

		label.hide();
		input.css('visibility','visible').attr('placeholder',value);
	});
*/

	// Setting up dropdown list
	$('.contactpage, .home-form').find('.contourField.dropdown').each(function() {
		var label = $(this).find('label');
		$(this).find('p.selected').html(label.html());
	});

	// Setting up checkbox list
	$('.contactpage .checkboxlist').each(function() {
		var list = $(this);

		$(this).find('label').unbind('click').click(function(e) {
			$(this).toggleClass('active');
		});

		$(this).find('input:checked').each(function() {
			$(this).next('label').addClass('active');
		});
	});

	var contactCount = 1;
	$('.contactoption').each(function() {
		$(this).addClass('option-'+contactCount);
		contactCount += 1;
	});

	$('form').each(function() {
		var form = $(this);
		var submit = $(this).find('input:submit');
		var recaptcha = $(this).find('.contourField.recaptcha');

		if(recaptcha.length > 0) {
			var recaptchaInput = recaptcha.find('input:text');
			submit.click(function() {
				if(!recaptchaInput.val()) {
					recaptcha.find('#recaptcha_widget_div').append('<p class="error">Please enter the security code</p>');
					recaptcha.find('.recaptchatable').addClass('error');
				}
			});
		}
	});

}

function setupDropdowns() {

	$('.dropdown').each(function() {
		var dropdown = $(this);

		// On selection
		dropdown.find('.dropdown-list a').click(function(e) {

			// Selecting option based on new list click
			var option = $(this).find('.value').html();
			var optionFormated = $(this).parent('li').data('type');
			dropdown.find('p.selected').attr('data-type',optionFormated).html(option);
			dropdown.find('.dropdown-list').stop().slideUp('fast');
			dropdown.find('.dropdown-arrow').removeClass('closed');

			return false;
		});

		// Opening selections
		dropdown.find('.dropdown-arrow').click(function(e) {
			$(this).toggleClass('closed');
			dropdown.find('.dropdown-list').stop().slideToggle('fast');
			return false;
		});
	});
}

function setupForms() {

	$('div.contourField.dropdown').each(function() {

		// --- Setup dropdown list --- //
		var dropdownList = $(this).find('> div');

		// Creating new dropdown list
		dropdownList.append('<div class="dropdown"></div>');
		var newDropDown = dropdownList.find('div.dropdown');

		// Setting arrow
		newDropDown.append('<a href="" class="dropdown-arrow"></a>');

		// Setting default selection
		newDropDown.append('<p class="selected"></p>');

		// Setting new drop down list items
		newDropDown.append('<ul class="dropdown-list"></ul>');
		dropdownList.find('select option').each(function() {
			var list = newDropDown.find('.dropdown-list');
			if($(this).val() != "") {
				list.append('<li class="option"><a href="">' + $(this).val() + '</a></li>');
			}
		});

		// Selecting option based on new list click
		newDropDown.find('.dropdown-list a').click(function(e) {
			var option = $(this).html();
			dropdownList.find('p.selected').html(option);
			dropdownList.find('select option').each(function() {
				if($(this).val() == option) {
					$(this).attr('selected',true);
				}
			});
			newDropDown.find('.dropdown-list').stop().slideUp('fast');
			newDropDown.find('.dropdown-arrow').removeClass('closed');
			return false;
		});

		// Opening selections
		newDropDown.find('.dropdown-arrow').click(function(e) {
			$(this).toggleClass('closed');
			newDropDown.find('.dropdown-list').stop().slideToggle('fast');
			return false;
		});
	});
	// --- Setup File Upload --- //
	var uploadTranslations = {
		'en-GB': ['Browse', 'Choose file'],
		'fr-FR': ['Parcourir', 'Choisir un fichier']
	};
	var currentLanguage = document.body.className.match(/language-(.{5})/)[1],
		uploadTranslation = uploadTranslations[currentLanguage];
	if (!uploadTranslation) {
		uploadTranslation = uploadTranslations['en-GB'];
	}
	$('div.fileupload input:file, div.perplexfileupload input:file').each(function() {
		$(this)
			.wrap('<div class="upload-file"></div>')
			.before('<span class="btn-primary standard green">' + uploadTranslation[0] + '</span><input id="uploadFile" placeholder="' + uploadTranslation[1] + '" disabled/>')
			.on("change", function(){
				$(this).siblings('#uploadFile').val($(this).val());
			})
			.hover(function() {
				$(this).siblings('.btn-green').toggleClass('over');
			});
	});
}

$(function() {

	// Firing functions
	setupHead();
	setupTopNav();
	setupBanners();
	setupHeights();
	setupSliders();
	setupFooter();
	setupPageNav();
	setupWaypoints();
	setupDropdowns();
	setupForms();
	setupCookieStatement();
	setup404();
	setupConsultancyPage();
	setupTeamPage();
	setupBlogPages();
	setupVacancies();
	setupCaseStudies();
	setupEvents();
	setupROI();
	setupContact();

	placeholders();

	// Based on https://github.com/Modernizr/Modernizr/blob/master/feature-detects/css/generatedcontent.js
	Modernizr.testStyles('#modernizr{font:0/0 a}#modernizr:after{content:":)";visibility:hidden;font:7px/1 a; line-height: 5rem}', function( node ) {
	    Modernizr.addTest('pseudoelementlineheightinrems', node.offsetHeight >= 10);
	});

	$(window).on('resize', function(){
        setupHeights();
    });

});

$(document).ready(function() {
    $("#mobile-menu").mmenu({
        navbars: [{
            position: "top",
            content: ['breadcrumbs', 'close']
        }],
        "dropdown": false
    });
    $.mmenu.configuration.classNames.navbars = {
        panelTitle: "Title",
        panelNext: "Next",
        panelPrev: "Prev"
    };
    $('.main-nav .inner ul .top-level').hover(function () {
        $(this).siblings().toggleClass('hoverEffect');
    });

    $('.home-slider-block').slick({
        adaptiveHeight: true
    });

    if (!($('.home-slider-block .slick-slide').length > 1)) {
        $('.slick-dots').hide();
    }

    $('.magnific-video a').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade mfp-video',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });
});

