﻿using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Identity
{
    public class ApplicationStore : UserStore<Users>
    {
        public ApplicationStore(ApplicationDbContext context) : base(context)
        {
        }
    }

    //Manager
    public class ApplicationUserManager : UserManager<Users>
    {
        public ApplicationUserManager(IUserStore<Users> store) : base(store)
        {
            UserValidator = new UserValidator<Users>(this)
            {
                AllowOnlyAlphanumericUserNames = false
            };
            DpapiDataProtectionProvider provider = new DpapiDataProtectionProvider("Masternaut");
            UserTokenProvider = new DataProtectorTokenProvider<Users>(provider.Create("EmailConfirmation"));

        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            return new ApplicationUserManager(new ApplicationStore(context.Get<ApplicationDbContext>()));
        }
    }

    //Roles
    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore) : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>()));
        }
    }
}
