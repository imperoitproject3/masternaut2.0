﻿using Masternaut.Core.Helper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Identity
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            string fromAddress = GlobalConfig.ProjectName + "<" + GlobalConfig.EmailUserName + ">";
            using (MailMessage mailMessage = new MailMessage(fromAddress, message.Destination))
            {
                try
                {
                    mailMessage.Subject = message.Subject;
                    mailMessage.Body = message.Body;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = GlobalConfig.SMTPClient;
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(GlobalConfig.EmailUserName, GlobalConfig.EmailPassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    await smtp.SendMailAsync(mailMessage);
                }
                catch (Exception err)
                {
                    string errorMessage = err.Message;
                }
            }
        }
    }
}
