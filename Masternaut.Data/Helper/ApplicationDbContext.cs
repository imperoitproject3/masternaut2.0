﻿using Masternaut.Data.Entities;
using Masternaut.Data.Migrations;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Helper
{
    public class ApplicationDbContext : IdentityDbContext<Users>
    {
        public ApplicationDbContext() : base("connectionString")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Users>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("RoleMaster");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
        }

        public virtual DbSet<IndustryType> IndustryType { get; set; }
        public virtual DbSet<IndustryLanguage> IndustryLanguage { get; set; }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<CategoryLanguage> CategoryLanguage { get; set; }

        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsLanguage> NewsLanguage { get; set; }

        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventLanguage> EventLanguage { get; set; }

        public virtual DbSet<Stories> Stories { get; set; }
        public virtual DbSet<StoriesLanguage> StoriesLanguage { get; set; }

        public virtual DbSet<ContactUs> ContactUs { get; set; }
        public virtual DbSet<JobApply> JobApply { get; set; }


    }
}
