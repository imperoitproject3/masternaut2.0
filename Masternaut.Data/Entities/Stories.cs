﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class Stories : BaseEntity
    {
        [Required]
        public string ImageName { get; set; }

        [Required]
        public long IndustryTypeId { get; set; }

        [ForeignKey("IndustryTypeId")]
        public virtual IndustryType IndustryType { get; set; }

        public virtual ICollection<StoriesLanguage> StoriesLanguage { get; set; }
    }
}
