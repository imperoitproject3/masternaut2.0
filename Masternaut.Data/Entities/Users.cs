﻿using Masternaut.Core.Enumerations;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class Users : IdentityUser
    {
        public string FirstName { get; set; }

        public UserRoles UserRole { get; set; }
    }
}
