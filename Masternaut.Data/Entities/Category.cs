﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class Category : BaseIdEntity
    {
        public virtual ICollection<CategoryLanguage> CategoryLanguage { get; set; }
    }
}
