﻿using Masternaut.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class NewsLanguage : BaseIdEntity
    {
        [Required]
        public string Title { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Description { get; set; }

        [EnumDataType(typeof(Language))]
        public Language Language { get; set; }

        [Required]
        public long NewsId { get; set; }

        [ForeignKey("NewsId")]
        public virtual News News { get; set; }
    }
}
