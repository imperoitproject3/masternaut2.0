﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class Event : BaseEntity
    {
        [Required]
        public string ImageName { get; set; }

        [Required]
        public DateTime StartDateUTC { get; set; }

        [Required]
        public DateTime EndDateUTC { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        public DbGeography GeoLocation { get; set; }

        public virtual ICollection<EventLanguage> EventLanguage { get; set; }
    }
}
