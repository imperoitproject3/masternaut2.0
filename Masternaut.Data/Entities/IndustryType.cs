﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class IndustryType : BaseIdEntity
    {
        public virtual ICollection<IndustryLanguage> IndustryLanguage { get; set; }
        public virtual ICollection<Stories> Stories { get; set; }

    }
}
