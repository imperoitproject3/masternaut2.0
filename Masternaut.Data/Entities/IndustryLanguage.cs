﻿using Masternaut.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class IndustryLanguage : BaseIdEntity
    {
        [Required]
        public string Title { get; set; }

        [EnumDataType(typeof(Language))]
        public Language Language { get; set; }

        public long IndustryId { get; set; }

        [ForeignKey("IndustryId")]
        public virtual IndustryType Industry { get; set; }
    }
}
