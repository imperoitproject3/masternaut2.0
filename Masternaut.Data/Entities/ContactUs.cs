﻿using Masternaut.Core.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Entities
{
    public class ContactUs : BaseEntity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string FeetSize { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Message { get; set; }

        [EnumDataType(typeof(ContactUsType))]
        public ContactUsType Type { get; set; }
    }
}
