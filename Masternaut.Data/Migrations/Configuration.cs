﻿using Masternaut.Core.Enumerations;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using Masternaut.Data.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;

namespace Masternaut.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));
            var userManager = new ApplicationUserManager(new ApplicationStore(context));

            #region Add Admin Role
            {
                var role = roleManager.FindByNameAsync(UserRoles.Admin.ToString()).Result;
                if (role == null)
                {
                    role = new IdentityRole(UserRoles.Admin.ToString());
                    var roleresult = roleManager.CreateAsync(role).Result;
                }
            }
            #endregion

            #region Add Mobile User Role
            {
                var role = roleManager.FindByNameAsync(UserRoles.MobileUser.ToString()).Result;
                if (role == null)
                {
                    role = new IdentityRole(UserRoles.MobileUser.ToString());
                    var roleresult = roleManager.CreateAsync(role).Result;
                }
            }
            #endregion

            #region Add admin as a admin@routehack.com
            {
                const string emailId = "info@Masternaut.be";
                const string userName = "MasternautAdmin";
                const string password = "Masternaut@123";
                var user = userManager.FindByNameAsync(userName).Result;
                if (user == null)
                {
                    user = new Users() { FirstName = "Admin", UserName = userName, Email = emailId, UserRole = UserRoles.Admin };
                    var result = userManager.CreateAsync(user, password).Result;
                    result = userManager.SetLockoutEnabledAsync(user.Id, false).Result;
                }

                var rolesForUser = userManager.GetRolesAsync(user.Id).Result;
                if (!rolesForUser.Contains(UserRoles.Admin.ToString()))
                {
                    var result = userManager.AddToRoleAsync(user.Id, UserRoles.Admin.ToString()).Result;
                }
            }
            #endregion
        }

    }
}
