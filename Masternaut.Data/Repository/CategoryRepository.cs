﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{

    public class CategoryRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public CategoryRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<AdminCategoryModel> GetAllCategory_IQueryable()
        {
            return (from c in _ctx.Category
                    select new AdminCategoryModel
                    {
                        Id = c.Id,
                        Categorylanguagemodellist = c.CategoryLanguage.Select(x => new CategoryLanguageModel
                        {
                            Language = x.Language,
                            LanguageName = x.Language.ToString(),
                            CategoryName = x.Title
                        }).ToList()
                    }).OrderBy(x => x.Id);
        }

        public IQueryable<DropdownModel> GetCategoryDropdown()
        {
            var selectedLang = Utilities.GetCurrentLanguage();
            return (from c in _ctx.CategoryLanguage
                    where c.Language == selectedLang
                    select new DropdownModel
                    {
                        Id = c.CategoryId,
                        Name = c.Title
                    });
        }

        public bool isCategoryDuplicate(AdminCategoryModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.Categorylanguagemodellist)
            {
                isDuplicate = _ctx.CategoryLanguage.Any(x => x.Language == item.Language && x.Title == item.CategoryName && x.CategoryId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<ResponseModel<object>> InsertOrUpdateCategory(AdminCategoryModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                bool isNew = false;
                var data = await (from c in _ctx.Category.Include("CategoryLanguage")
                                  where c.Id == model.Id
                                  select c).FirstOrDefaultAsync();
                if (data == null)
                {
                    isNew = true;
                    data = new Category();
                }
                if (isNew)
                {
                    List<CategoryLanguage> lstCategoryLanguage = new List<CategoryLanguage>();
                    foreach (var item in model.Categorylanguagemodellist)
                    {
                        lstCategoryLanguage.Add(new CategoryLanguage
                        {
                            CategoryId = data.Id,
                            Title = item.CategoryName,
                            Language = item.Language
                        });
                    }
                    data.CategoryLanguage = lstCategoryLanguage;
                    _ctx.Category.Add(data);
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    foreach (var item in data.CategoryLanguage)
                    {
                        CategoryLanguageModel categoryLanguage = model.Categorylanguagemodellist.FirstOrDefault(x => x.Language == item.Language);
                        item.Title = categoryLanguage.CategoryName;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Category saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteCategory(long CategoryId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                Category data = await _ctx.Category.FirstOrDefaultAsync(x => x.Id == CategoryId);
                if (data != null)
                {
                    _ctx.Category.Remove(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Category deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Category found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<AdminCategoryModel> GetCategoryDetail(long CatrgoryId)
        {
            return await (from c in _ctx.Category
                          where c.Id == CatrgoryId
                          select new AdminCategoryModel
                          {
                              Id = c.Id,
                              Categorylanguagemodellist = c.CategoryLanguage.Select(x => new CategoryLanguageModel
                              {
                                  Language = x.Language,
                                  CategoryName = x.Title,
                                  LanguageName = x.Language.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
