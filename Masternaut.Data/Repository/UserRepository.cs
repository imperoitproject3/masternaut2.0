﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using Masternaut.Data.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{
    public class UserRepository : IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IOwinContext _owin;

        public UserRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<Users>(_ctx));
        }

        public UserRepository(IOwinContext owin)
        {
            _owin = owin;
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<Users>(_ctx));
            _userManager.EmailService = new EmailService();
            _authenticationManager = owin.Authentication;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByEmailAsync(model.EmailId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid email address.";
            }
            else
            {
                bool validRole = await _userManager.IsInRoleAsync(user.Id, user.UserRole.ToString());

                if (validRole)
                {
                    bool validPassword = await _userManager.CheckPasswordAsync(user, model.Password);
                    if (validPassword)
                    {
                        await SignInAsync(user, false, false);

                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = string.Format("User {0} Logged in successfully!", user.UserName);
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                var user = await _userManager.FindByIdAsync(model.UserId);
                if (user == null)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid username or password.";
                }
                else
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Password changed successfully";
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        private async Task SignInAsync(Users user, bool isPersistent, bool rememberBrowser)
        {
            WebLogout();
            var userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("FullName", user.UserName));
            _authenticationManager.SignIn(
                new AuthenticationProperties { IsPersistent = isPersistent },
                userIdentity);
        }

        public void WebLogout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
               DefaultAuthenticationTypes.TwoFactorCookie);
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
