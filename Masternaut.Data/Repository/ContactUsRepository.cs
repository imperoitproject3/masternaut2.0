﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Core.Resources;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using Masternaut.Data.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{
    public class ContactUsRepository
    {
        private readonly ApplicationDbContext _ctx;
        public ContactUsRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<ResponseModel<object>> SaveContactUs(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ContactUs data = new ContactUs()
                {
                    Email = model.Email,
                    FeetSize = model.FleetSize,
                    Name = model.Name,
                    PhoneNumber = model.Phone,
                    Type = model.Type,
                    Message = model.Message,
                };
                _ctx.ContactUs.Add(data);
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.InquirySuccess;

                StringBuilder mailContent = new StringBuilder();
                mailContent.Append("Person With Name: " + model.Name + " has contacted you on your website www.Masternaut.be");
                mailContent.Append("<BR/>");
                mailContent.Append("<ul>");
                mailContent.Append("<li>Phone number: " + model.Phone + "</li>");
                mailContent.Append("<li>Email: " + model.Email + "</li>");
                mailContent.Append("<li>Message: " + model.Message + "</li>");
                mailContent.Append("<li>Feet Size: " + model.FleetSize + "</li>");
                mailContent.Append("</ul>");
                mailContent.Append("<BR/>");
                mailContent.Append("<BR/>");
                mailContent.Append("Thank You!");

                IdentityMessage message = new IdentityMessage();
                message.Subject = "New Inquiry";
                message.Body = mailContent.ToString();
                message.Destination = "Info@masternaut.be";
                await new EmailService().SendAsync(message);

                string Subject = "Inquiry Send Successfully.";
                string Heading = "Hello,";
                string EmailContentLine1 = "We have registered your request.";
                string EmailContentLine2 = "You will receive a message as soon as your request is processed.";
                string Regards = "With best regards,";
                var currentla = Utilities.GetCurrentLanguage();

                switch (currentla)
                {
                    case Language.English:
                        Subject = "Inquiry Send Successfully.";
                        Heading = "Hello,";
                        EmailContentLine1 = "We have registered your request.";
                        EmailContentLine2 = "You will receive a message as soon as your request is processed.";
                        Regards = "With best regards,";
                        break;
                    case Language.Dutch:
                        Subject = "Onderzoek met succes verzonden.";
                        Heading = "Geachte heer, mevrouw,";
                        EmailContentLine1 = "Wij nemen uw e-mail zo snel mogelijk in behandeling. ";
                        EmailContentLine2 = "U ontvangt bericht van ons zodra uw verzoek is afgehandeld.";
                        Regards = "Met vriendelijke groeten,";
                        break;
                    case Language.French:
                        Subject = "Enquête envoyée avec succès.";
                        Heading = "Bonjour,";
                        EmailContentLine1 = "Nous avons bien enregistré votre demande.";
                        EmailContentLine2 = "You will receive a message as soon as your request is processed.";
                        Regards = "With best regards,";
                        break;
                    default:
                        Subject = "Onderzoek met succes verzonden.";
                        Heading = "Geachte heer, mevrouw,";
                        EmailContentLine1 = "Wij nemen uw e-mail zo snel mogelijk in behandeling. ";
                        EmailContentLine2 = "U ontvangt bericht van ons zodra uw verzoek is afgehandeld.";
                        Regards = "Met vriendelijke groeten,";
                        break;

                }

                StringBuilder mailContent2 = new StringBuilder();
                mailContent2.Append("<BR/>");
                mailContent2.Append(Heading);
                mailContent2.Append("<BR/>");
                mailContent2.Append(EmailContentLine1);
                mailContent2.Append("<BR/>");
                mailContent2.Append(EmailContentLine2);
                mailContent2.Append("<BR/>");
                mailContent2.Append("<BR/>");
                mailContent2.Append(Regards);
                mailContent2.Append("<BR/>");
                mailContent2.Append("Masternaut team");

                IdentityMessage message2 = new IdentityMessage();
                message2.Subject = Subject;
                message2.Body = 0.ToString();
                message2.Destination = model.Email;
                await new EmailService().SendAsync(message2);

            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SaveJobApply(JobApplyModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                JobApply data = new JobApply()
                {
                    Email = model.Email,
                    Name = model.Name,
                    FileName = model.FileName,
                    JobappliedFor = model.JobappliedFor,
                };
                _ctx.JobApply.Add(data);
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = ResponseMessages.JobApplySuccessMessage;

            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public IQueryable<ContactUsViewModel> GetAllContactUs_IQueryable()
        {
            return (from c in _ctx.ContactUs
                    where c.Type != ContactUsType.Brochure
                    select new ContactUsViewModel
                    {
                        Id = c.Id,
                        Email = c.Email,
                        FleetSize = c.FeetSize,
                        Message = c.Message,
                        Name = c.Name,
                        Phone = c.PhoneNumber,
                        Type = c.Type,
                        DateTime = c.CreatedDate
                    }).OrderByDescending(x => x.Id);
        }

        public IQueryable<ContactUsViewModel> GetAllBrochure_IQueryable()
        {
            return (from c in _ctx.ContactUs
                    where c.Type == ContactUsType.Brochure
                    select new ContactUsViewModel
                    {
                        Id = c.Id,
                        Email = c.Email,
                        FleetSize = c.FeetSize,
                        Message = c.Message,
                        Name = c.Name,
                        Phone = c.PhoneNumber,
                        Type = c.Type,
                        DateTime = c.CreatedDate
                    }).OrderByDescending(x => x.Id);
        }

        public IQueryable<JobApplyViewModel> GetAllJobApply_IQueryable()
        {
            return (from c in _ctx.JobApply
                    select new JobApplyViewModel
                    {
                        Id = c.Id,
                        Email = c.Email,
                        Name = c.Name,
                        FileName = c.FileName,
                        JobappliedFor = c.JobappliedFor,
                        DateTime = c.CreatedDate
                    }).OrderByDescending(x => x.Id);
        }
    }
}
