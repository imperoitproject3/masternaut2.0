﻿using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Data.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{
    public class IndustryTypeRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public IndustryTypeRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<DropdownModel> GetIndustryDropdown()
        {
            return (from c in _ctx.IndustryLanguage
                    where c.Language == GlobalConfig.DefaultLanguageId
                    select new DropdownModel
                    {
                        Id = c.IndustryId,
                        Name = c.Title
                    });
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
