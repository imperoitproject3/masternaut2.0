﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{
    public class NewsRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public NewsRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<NewsViewModel> GetAllNews_IQueryable()
        {
            return (from c in _ctx.News
                    select new NewsViewModel
                    {
                        Id = c.Id,
                        imageName = c.ImageName,
                        CategoryName = c.Category.CategoryLanguage.FirstOrDefault(x => x.Language == GlobalConfig.DefaultLanguageId).Title,
                        Newslanguagemodellist = c.NewsLanguage.Select(x => new NewsLanguageModel
                        {
                            Language = x.Language,
                            LanguageName = x.Language.ToString(),
                            Title = x.Title,
                            Description = x.Description,
                        }).ToList(),
                        DateTime = c.CreatedDate
                    }).OrderByDescending(x => x.Id);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateNews(NewsAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await (from c in _ctx.News.Include("NewsLanguage")
                                  where c.Id == model.Id
                                  select c).FirstOrDefaultAsync();
                if (data == null)
                {
                    isNew = true;
                    data = new News();
                }
                data.ImageName = model.imageName ?? data.ImageName;
                data.CategoryId = model.CategoryId;
                data.CreatedDate = model.DateTime;
                if (isNew)
                {
                    List<NewsLanguage> lstNewsLanguage = new List<NewsLanguage>();
                    foreach (var item in model.Newslanguagemodellist)
                    {
                        lstNewsLanguage.Add(new NewsLanguage
                        {
                            NewsId = data.Id,
                            Title = item.Title,
                            Description = item.Description,
                            Language = item.Language
                        });
                    }
                    data.NewsLanguage = lstNewsLanguage;
                    _ctx.News.Add(data);
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    foreach (var item in data.NewsLanguage)
                    {
                        NewsLanguageModel newsLanguage = model.Newslanguagemodellist.FirstOrDefault(x => x.Language == item.Language);
                        item.Title = newsLanguage.Title;
                        item.Description = newsLanguage.Description;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "News saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<NewsAddUpdateModel> GetNewsDetail(long Id)
        {
            return await (from c in _ctx.News
                          where c.Id == Id
                          select new NewsAddUpdateModel
                          {
                              Id = c.Id,
                              imageName = c.ImageName,
                              CategoryId = c.CategoryId,
                              Newslanguagemodellist = c.NewsLanguage.Select(x => new NewsLanguageModel
                              {
                                  Language = x.Language,
                                  Title = x.Title,
                                  Description = x.Description,
                                  LanguageName = x.Language.ToString(),
                              }).ToList(),
                              DateTime = c.CreatedDate
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var data = await _ctx.News.FirstOrDefaultAsync(x => x.Id == Id);
                if (data != null)
                {
                    _ctx.News.Remove(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "News deleted successfully!";
                }
                else
                {
                    mResult.Message = "No news found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<List<SingleNewsViewModel>> GetAllNews(PaginationModel model)
        {
            var selectedLang = Utilities.GetCurrentLanguage();
            var NewsList = await (from c in _ctx.NewsLanguage
                                  where c.Language == selectedLang
                                  && (c.News.CategoryId == model.Id || model.Id == 0)
                                  select new SingleNewsViewModel
                                  {
                                      Id = c.NewsId,
                                      ImageName = c.News.ImageName,
                                      Description = c.Description,
                                      Title = c.Title,
                                      CreatedDate = c.News.CreatedDate
                                  })
                               .OrderByDescending(x => x.Id)
                               .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize)
                               .ToListAsync();
            if (NewsList != null)
            {
                foreach (var item in NewsList)
                {
                    item.Description = Utilities.StripHtml(item.Description);
                }
            }
            return NewsList;
        }

        public async Task<List<NewsTitle>> GetlatestNews()
        {
            var selectedLang = Utilities.GetCurrentLanguage();
            var NewsList = await (from c in _ctx.NewsLanguage
                                  where c.Language == selectedLang
                                  orderby c.News.CreatedDate descending
                                  select new NewsTitle
                                  {
                                      Id = c.NewsId,
                                      Title = c.Title,
                                  }).Take(5).ToListAsync();
            return NewsList;
        }

        public async Task<SingleNewsViewModel> GetNewsDetailsForWeb(long Id)
        {
            SingleNewsViewModel model = new SingleNewsViewModel();
            var selectedLang = Utilities.GetCurrentLanguage();
            var objNews = await (from c in _ctx.NewsLanguage
                                 where c.Language == selectedLang
                                 && c.NewsId == Id
                                 select new SingleNewsViewModel
                                 {
                                     Id = c.NewsId,
                                     ImageName = c.News.ImageName,
                                     Description = c.Description,
                                     Title = c.Title,
                                     CreatedDate = c.News.CreatedDate
                                 }).FirstOrDefaultAsync();
            if (objNews != null)
            {
                model = objNews;
                var Ids = _ctx.News.Select(x => x.Id).ToList();
                long? nextRecord = Ids.SkipWhile(i => i != model.Id).Skip(1).FirstOrDefault();
                model.NextRecordId = nextRecord ?? 0;
            }
            model.LatestNewsList = await GetlatestNews();
            return model;
        }

        public async Task<int> GetTotalNewsCount()
        {
            return await _ctx.News.CountAsync();
        }


        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
