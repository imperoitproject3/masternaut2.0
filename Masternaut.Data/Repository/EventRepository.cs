﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{
    public class EventRepository
    {
        private ApplicationDbContext _ctx;

        public EventRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<EventViewModel> GetAllEvent_IQueryable()
        {
            return (from c in _ctx.Event
                    select new EventViewModel
                    {
                        Id = c.Id,
                        imageName = c.ImageName,
                        StartTime = c.StartDateUTC,
                        EndTIme = c.EndDateUTC,
                        Location = c.Location,
                        Eventlanguagemodellist = c.EventLanguage.Select(x => new EventLanguageModel
                        {
                            Language = x.Language,
                            LanguageName = x.Language.ToString(),
                            Title = x.Title,
                            Description = x.Description,
                        }).ToList()
                    }).OrderByDescending(x => x.Id);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateEvent(EventAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await (from c in _ctx.Event.Include("EventLanguage")
                                  where c.Id == model.Id
                                  select c).FirstOrDefaultAsync();
                if (data == null)
                {
                    isNew = true;
                    data = new Event();
                }
                data.ImageName = model.imageName ?? data.ImageName;
                data.StartDateUTC = model.StartTime;
                data.EndDateUTC = model.EndTIme;
                data.Location = model.Location;
                data.GeoLocation = Data_Utility.CreatePoint(model.LocationLatitude, model.LocationLongitude);
                if (isNew)
                {
                    List<EventLanguage> lstEventLanguage = new List<EventLanguage>();
                    foreach (var item in model.Eventlanguagemodellist)
                    {
                        lstEventLanguage.Add(new EventLanguage
                        {
                            EventId = data.Id,
                            Title = item.Title,
                            Description = item.Description,
                            Language = item.Language
                        });
                    }
                    data.EventLanguage = lstEventLanguage;
                    _ctx.Event.Add(data);
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    foreach (var item in data.EventLanguage)
                    {
                        var eventLanguage = model.Eventlanguagemodellist.FirstOrDefault(x => x.Language == item.Language);
                        item.Title = eventLanguage.Title;
                        item.Description = eventLanguage.Description;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Event saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<EventAddUpdateModel> GetEventDetail(long Id)
        {
            return await (from c in _ctx.Event
                          where c.Id == Id
                          select new EventAddUpdateModel
                          {
                              Id = c.Id,
                              imageName = c.ImageName,
                              EndTIme = c.EndDateUTC,
                              Location = c.Location,
                              StartTime = c.StartDateUTC,
                              LocationLatitude = c.GeoLocation.Latitude ?? 0,
                              LocationLongitude = c.GeoLocation.Longitude ?? 0,
                              Eventlanguagemodellist = c.EventLanguage.Select(x => new EventLanguageModel
                              {
                                  Language = x.Language,
                                  Title = x.Title,
                                  Description = x.Description,
                                  LanguageName = x.Language.ToString(),
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var data = await _ctx.Event.FirstOrDefaultAsync(x => x.Id == Id);
                if (data != null)
                {
                    _ctx.Event.Remove(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Event deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Event found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<ViewAllEventModel> GetEventList()
        {
            ViewAllEventModel model = new ViewAllEventModel();
            var todayNowUtc = Utility.GetSystemDateTimeUTC();
            var selectedLang = Utilities.GetCurrentLanguage();
            var UpcomingEventList = await (from evnt in _ctx.EventLanguage
                                           where evnt.Language == selectedLang
                                           && evnt.Event.StartDateUTC >= todayNowUtc
                                           orderby evnt.Event.StartDateUTC descending
                                           select new SingleEventViewModel
                                           {
                                               Id = evnt.EventId,
                                               Title = evnt.Title,
                                               Description = evnt.Description,
                                               ImageName = evnt.Event.ImageName,
                                               EndDateTIme = evnt.Event.EndDateUTC,
                                               StartDateTime = evnt.Event.StartDateUTC,
                                               Location = evnt.Event.Location
                                           }).ToListAsync();

            var PastEventList = await (from evnt in _ctx.EventLanguage
                                       where evnt.Language == selectedLang
                                       && evnt.Event.StartDateUTC <= todayNowUtc
                                       orderby evnt.Event.StartDateUTC descending
                                       select new SingleEventViewModel
                                       {
                                           Id = evnt.EventId,
                                           Title = evnt.Title,
                                           Description = evnt.Description,
                                           ImageName = evnt.Event.ImageName,
                                           EndDateTIme = evnt.Event.EndDateUTC,
                                           StartDateTime = evnt.Event.StartDateUTC,
                                           Location = evnt.Event.Location
                                       }).ToListAsync();
            if (UpcomingEventList != null)
            {
                foreach (var item in UpcomingEventList)
                {
                    item.Description = Utilities.StripHtml(item.Description);
                }
            }
            if (PastEventList != null)
            {
                foreach (var item in PastEventList)
                {
                    item.Description = Utilities.StripHtml(item.Description);
                }
            }
            model.PastEventList = PastEventList;
            model.UpcomingEventList = UpcomingEventList;
            return model;
        }

        public async Task<SingleEventViewModel> GetEventDetailsForWeb(long Id)
        {
            var selectedLang = Utilities.GetCurrentLanguage();
            var evntdetails = await (from evnt in _ctx.EventLanguage
                                     where evnt.Language == selectedLang
                                     && evnt.EventId == Id
                                     select new SingleEventViewModel
                                     {
                                         Id = evnt.EventId,
                                         Title = evnt.Title,
                                         Description = evnt.Description,
                                         ImageName = evnt.Event.ImageName,
                                         EndDateTIme = evnt.Event.EndDateUTC,
                                         StartDateTime = evnt.Event.StartDateUTC,
                                         Location = evnt.Event.Location,
                                         LocationLatitude = evnt.Event.GeoLocation.Latitude ?? 0,
                                         LocationLongitude = evnt.Event.GeoLocation.Longitude ?? 0
                                     }).FirstOrDefaultAsync();
            return evntdetails;
        }

        public async Task<int> GetTotalEventCount()
        {
            return await _ctx.Event.CountAsync();
        }
    }
}
