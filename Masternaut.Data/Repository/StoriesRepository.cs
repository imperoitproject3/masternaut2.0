﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using Masternaut.Data.Entities;
using Masternaut.Data.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Data.Repository
{
    public class StoriesRepository : IDisposable
    {
        private ApplicationDbContext _ctx;
        public StoriesRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<StoriesViewModel> GetAllStories_IQueryable()
        {
            return (from c in _ctx.Stories
                    select new StoriesViewModel
                    {
                        Id = c.Id,
                        imageName = c.ImageName,
                        IndustryType = c.IndustryType.IndustryLanguage.FirstOrDefault(x => x.Language == GlobalConfig.DefaultLanguageId).Title,
                        StoriesLanguagemodellist = c.StoriesLanguage.Select(x => new StoriesLanguageModel
                        {
                            Language = x.Language,
                            LanguageName = x.Language.ToString(),
                            Title = x.Title,
                            Description = x.Description,
                        }).ToList()
                    }).OrderByDescending(x => x.Id);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateStories(StoriesAddUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await (from c in _ctx.Stories.Include("StoriesLanguage")
                                  where c.Id == model.Id
                                  select c).FirstOrDefaultAsync();
                if (data == null)
                {
                    isNew = true;
                    data = new Stories();
                }
                data.ImageName = model.imageName ?? data.ImageName;
                data.IndustryTypeId = model.IndustryTypeId;
                if (isNew)
                {
                    List<StoriesLanguage> lstStoriesLanguage = new List<StoriesLanguage>();
                    foreach (var item in model.StoriesLanguagemodellist)
                    {
                        lstStoriesLanguage.Add(new StoriesLanguage
                        {
                            StoriesId = data.Id,
                            Title = item.Title.Trim(),
                            Description = item.Description,
                            Language = item.Language
                        });
                    }
                    data.StoriesLanguage = lstStoriesLanguage;
                    _ctx.Stories.Add(data);
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    foreach (var item in data.StoriesLanguage)
                    {
                        var StoriesLanguage = model.StoriesLanguagemodellist.FirstOrDefault(x => x.Language == item.Language);
                        item.Title = StoriesLanguage.Title;
                        item.Description = StoriesLanguage.Description;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Stories saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<StoriesAddUpdateModel> GetStoriesDetail(long Id)
        {
            return await (from c in _ctx.Stories
                          where c.Id == Id
                          select new StoriesAddUpdateModel
                          {
                              Id = c.Id,
                              imageName = c.ImageName,
                              IndustryTypeId = c.IndustryTypeId,
                              StoriesLanguagemodellist = c.StoriesLanguage.Select(x => new StoriesLanguageModel
                              {
                                  Language = x.Language,
                                  Title = x.Title,
                                  Description = x.Description,
                                  LanguageName = x.Language.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var data = await _ctx.Stories.FirstOrDefaultAsync(x => x.Id == Id);
                if (data != null)
                {
                    _ctx.Stories.Remove(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Stories deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Stories found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<List<SingleStoriesViewModel>> GetTopStories()
        {
            var selectedLang = Utilities.GetCurrentLanguage();
            var objStories = await (from c in _ctx.StoriesLanguage
                                    where c.Language == selectedLang
                                    select new SingleStoriesViewModel
                                    {
                                        Id = c.StoriesId,
                                        ImageName = c.Stories.ImageName,
                                        IndustryType = c.Stories.IndustryType.IndustryLanguage.FirstOrDefault(x => x.Language == selectedLang).Title,
                                        Description = c.Description,
                                        Title = c.Title
                                    }).Take(3).ToListAsync();
            if (objStories != null)
            {
                foreach (var item in objStories)
                {
                    item.Description = Utilities.StripHtml(item.Description);
                }
            }
            return objStories;
        }

        public async Task<CustomerStoriesViewModel> GetAllStories()
        {
            CustomerStoriesViewModel model = new CustomerStoriesViewModel();
            var selectedLang = Utilities.GetCurrentLanguage();
            var objIndustryType = await (from ind in _ctx.IndustryLanguage
                                         where ind.Language == selectedLang
                                         select new IndustryTypeModel
                                         {
                                             Id = ind.IndustryId,
                                             Title = ind.Title,
                                             Count = ind.Industry.Stories.LongCount(),
                                         }).ToListAsync();

            var objStories = await (from c in _ctx.StoriesLanguage
                                    where c.Language == selectedLang
                                    select new SingleStoriesViewModel
                                    {
                                        Id = c.StoriesId,
                                        ImageName = c.Stories.ImageName,
                                        IndustryType = c.Stories.IndustryType.IndustryLanguage.FirstOrDefault(x => x.Language == selectedLang).Title,
                                        Description = c.Description,
                                        Title = c.Title,
                                        IndustryTypeId = c.Stories.IndustryTypeId
                                    }).ToListAsync();

            if (objStories != null)
            {
                foreach (var item in objStories)
                {
                    item.Description = Utilities.StripHtml(item.Description);
                }
                model.Stories = objStories;
            }
            if (objIndustryType != null)
                model.IndustryType = objIndustryType;
            return model;
        }

        public async Task<SingleStoriesViewModel> GetStoriesDetails(long Id)
        {
            var selectedLang = Utilities.GetCurrentLanguage();
            var objStories = await (from stories in _ctx.StoriesLanguage
                                    where stories.StoriesId == Id
                                    && stories.Language == selectedLang
                                    select new SingleStoriesViewModel
                                    {
                                        Id = stories.Id,
                                        Description = stories.Description,
                                        ImageName = stories.Stories.ImageName,
                                        IndustryTypeId = stories.Stories.IndustryTypeId,
                                        Title = stories.Title
                                    }).FirstOrDefaultAsync();

            if (objStories != null)
            {
                objStories.RelatedStories = await (from a in _ctx.StoriesLanguage
                                                   where a.Stories.IndustryTypeId == objStories.IndustryTypeId
                                                   && a.Id != objStories.Id
                                                   && a.Language == selectedLang
                                                   select new SingleStoriesViewModel
                                                   {
                                                       Id = a.StoriesId,
                                                       ImageName = a.Stories.ImageName,
                                                       Title = a.Title
                                                   }).Take(5).ToListAsync();
            }
            return objStories;
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
