﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class AccountController : Controller
    {
        private UserService _service;
        public AccountController()
        {
            _service = new UserService();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            AdminLoginModel model = new AdminLoginModel();
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AdminLoginModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<object> mResult = await _service.AdminLogin(model);
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger, mResult.Message);
                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToAction("Index", "Home");
                ModelState.AddModelError("", mResult.Message);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(AdminChangePasswordModel model)
        {
            _service = new UserService(Request.GetOwinContext());
            ResponseModel<object> mResult = await _service.AdminChangePassword(model);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger, mResult.Message);
            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            _service = new UserService(Request.GetOwinContext());
            _service.WebLogout();
            return RedirectToActionPermanent("Login", "Account");
        }

    }
}