﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            OnInitBreadcrumb();
        }

        public void OnBindMessage(ResponseStatus status, string message)
        {
            OnBindMessage((status == ResponseStatus.Success ? AlertStatus.Success : AlertStatus.Danger), message);
        }

        public void OnBindMessage(AlertStatus status, string message)
        {
            TempData["alertModel"] = new AlertModel(status, message);
        }

        public void OnInitBreadcrumb()
        {
            string actionName = ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = ControllerContext.RouteData.Values["controller"].ToString();

            if (controllerName != "Home")
            {
                if (actionName == "Index")
                {
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        subActionTitle = controllerName
                    };
                }
                else
                {
                    ViewBag.Action = actionName;
                    ViewBag.breadcrumbModel = new BreadcrumbModel()
                    {
                        actioncontroller = controllerName,
                        actionName = "Index",
                        actionTitle = controllerName,
                        subActionTitle = actionName
                    };
                }
            }
        }

        public void BindCategory()
        {
            using (CategoryService service = new CategoryService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetCategoryDropdown().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["CategoryList"] = dropDownList;
            }
        }

        public void BindIndustryType()
        {
            using (IndustryTypeService service = new IndustryTypeService())
            {
                List<SelectListItem> dropDownList = (from x in service.GetIndustryDropdown().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.Id.ToString()
                                                     }).ToList();
                ViewData["IndustryTypeSList"] = dropDownList;
            }
        }

        public void BindLanguage()
        {
            var Languagelist = new List<SelectListItem>();
            Languagelist.Add(new SelectListItem { Text = Language.English.ToString(), Value = Language.English.ToString() });
            Languagelist.Add(new SelectListItem { Text = Language.Dutch.ToString(), Value = Language.Dutch.ToString() });
            Languagelist.Add(new SelectListItem { Text = Language.French.ToString(), Value = Language.French.ToString() });

            ViewData["LanguageList"] = Languagelist;
        }
    }
}