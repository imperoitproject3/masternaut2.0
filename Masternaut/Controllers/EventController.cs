﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class EventController : BaseController
    {
        private EventService _service;
        ResponseModel<object> mResult;
        public EventController()
        {
            _service = new EventService();
        }

        public ActionResult Index()
        {
            IQueryable<EventViewModel> objEvent = _service.GetAllEvent_IQueryable();
            return View(objEvent);
        }

        public async Task<ActionResult> Add()
        {
            EventAddUpdateModel model = new EventAddUpdateModel();
            List<EventLanguageModel> NewsLanguages = new List<EventLanguageModel>();
            NewsLanguages.Add(new EventLanguageModel()
            {
                Language = Language.English,
                LanguageName = Language.English.ToString(),
            });

            NewsLanguages.Add(new EventLanguageModel()
            {
                Language = Language.Dutch,
                LanguageName = Language.Dutch.ToString(),
            });

            NewsLanguages.Add(new EventLanguageModel()
            {
                Language = Language.French,
                LanguageName = Language.French.ToString(),
            });

            model.Eventlanguagemodellist = NewsLanguages;
            return View("AddorUpdate", model);
        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> Add(EventAddUpdateModel model)
        {
            if (ModelState.IsValid)
            {
                mResult = await _service.InsertOrUpdateEvent(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            BindCategory();
            var model = await _service.GetEventDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EventAddUpdateModel model)
        {
            BindCategory();
            if (ModelState.IsValid)
            {
                mResult = await _service.InsertOrUpdateEvent(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(long Id)
        {
            mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}