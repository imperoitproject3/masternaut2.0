﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class NewsController : BaseController
    {
        private NewsService _service;
        ResponseModel<object> mResult;

        public NewsController()
        {
            _service = new NewsService();
        }
        public ActionResult Index()
        {
            IQueryable<NewsViewModel> objNews = _service.GetAllNews_IQueryable();
            return View(objNews);
        }

        public async Task<ActionResult> Add()
        {
            BindCategory();
            NewsAddUpdateModel model = new NewsAddUpdateModel();
            List<NewsLanguageModel> NewsLanguages = new List<NewsLanguageModel>();
            NewsLanguages.Add(new NewsLanguageModel()
            {
                Language = Language.English,
                LanguageName = Language.English.ToString(),
            });

            NewsLanguages.Add(new NewsLanguageModel()
            {
                Language = Language.Dutch,
                LanguageName = Language.Dutch.ToString(),
            });

            NewsLanguages.Add(new NewsLanguageModel()
            {
                Language = Language.French,
                LanguageName = Language.French.ToString(),
            });

            model.Newslanguagemodellist = NewsLanguages;
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(NewsAddUpdateModel model)
        {
            BindCategory();
            if (ModelState.IsValid)
            {

                mResult = await _service.InsertOrUpdateNews(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            BindCategory();
            var model = await _service.GetNewsDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(NewsAddUpdateModel model)
        {
            BindCategory();
            if (ModelState.IsValid)
            {
                mResult = await _service.InsertOrUpdateNews(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<JsonResult> Delete(long Id)
        {
            mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}