﻿using Masternaut.Core.Helper;
using Masternaut.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class UtilityController : Controller
    {
        [HttpPost]
        public JsonResult UploadCropImage(SaveImageModel model)
        {
            string imageName = Guid.NewGuid().ToString() + "." + model.extension;
            string filePath = Path.Combine(model.saveFilePath, imageName);
            ResponseModel<object> mResult = Utilities.SaveCropImage(model.imgBase64String, filePath, model.extension);
            mResult.Result = imageName;

            if (!string.IsNullOrEmpty(model.previousImageName))
            {
                string ExistFilePath = Path.Combine(model.saveFilePath, model.previousImageName);
                if (System.IO.File.Exists(ExistFilePath))
                {
                    System.IO.File.Delete(ExistFilePath);
                }
            }

            return Json(mResult);
        }
    }
}