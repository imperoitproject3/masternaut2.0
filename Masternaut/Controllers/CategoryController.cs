﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class CategoryController : BaseController
    {
        private CategoryService _service;

        ResponseModel<object> mResult;

        public CategoryController()
        {
            _service = new CategoryService();
        }
        public ActionResult Index()
        {
            IQueryable<AdminCategoryModel> objCategories = _service.GetAllCategory_IQueryable();
            return View(objCategories);
        }

        public async Task<ActionResult> Add()
        {
            AdminCategoryModel model = new AdminCategoryModel();
            List<CategoryLanguageModel> CategoryLanguages = new List<CategoryLanguageModel>();
            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                Language = Language.English,
                LanguageName = Language.English.ToString(),
            });

            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                Language = Language.Dutch,
                LanguageName = Language.Dutch.ToString(),
            });

            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                Language = Language.French,
                LanguageName = Language.French.ToString(),
            });

            model.Categorylanguagemodellist = CategoryLanguages;
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AdminCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.isCategoryDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.Danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    mResult = await _service.InsertOrUpdateCategory(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            AdminCategoryModel model = await _service.GetCategoryDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(AdminCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.isCategoryDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.Danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    mResult = await _service.InsertOrUpdateCategory(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("AddorUpdate", model);
        }

        public async Task<JsonResult> Delete(long Id)
        {
            mResult = await _service.DeleteCategory(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}