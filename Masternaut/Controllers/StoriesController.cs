﻿using Masternaut.Core.Enumerations;
using Masternaut.Core.Model;
using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class StoriesController : BaseController
    {
        private StoriesService _service;
        ResponseModel<object> mResult;

        public StoriesController()
        {
            _service = new StoriesService();
        }

        public ActionResult Index()
        {
            IQueryable<StoriesViewModel> objStories = _service.GetAllStories_IQueryable();
            return View(objStories);
        }

        public async Task<ActionResult> Add()
        {
            BindIndustryType();
            StoriesAddUpdateModel model = new StoriesAddUpdateModel();
            List<StoriesLanguageModel> StoriesLanguages = new List<StoriesLanguageModel>();
            StoriesLanguages.Add(new StoriesLanguageModel()
            {
                Language = Language.English,
                LanguageName = Language.English.ToString(),
            });

            StoriesLanguages.Add(new StoriesLanguageModel()
            {
                Language = Language.Dutch,
                LanguageName = Language.Dutch.ToString(),
            });

            StoriesLanguages.Add(new StoriesLanguageModel()
            {
                Language = Language.French,
                LanguageName = Language.French.ToString(),
            });

            model.StoriesLanguagemodellist = StoriesLanguages;
            return View("AddorUpdate", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Add(StoriesAddUpdateModel model)
        {
            BindIndustryType();

            if (ModelState.IsValid)
            {

                mResult = await _service.InsertOrUpdateStories(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            BindIndustryType();
            var model = await _service.GetStoriesDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddorUpdate", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(StoriesAddUpdateModel model)
        {
            BindIndustryType();

            if (ModelState.IsValid)
            {
                mResult = await _service.InsertOrUpdateStories(model);
                OnBindMessage(mResult.Status, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Index");
            }
            return View("AddorUpdate", model);
        }
        [HttpPost]
        public async Task<JsonResult> Delete(long Id)
        {
            mResult = await _service.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}