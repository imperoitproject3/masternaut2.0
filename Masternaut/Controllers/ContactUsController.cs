﻿using Masternaut.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Masternaut.Controllers
{
    public class ContactUsController : Controller
    {
        private readonly ContactUsService _service;
        public ContactUsController()
        {
            _service = new ContactUsService();
        }

        public ActionResult Support()
        {
            var ObjData = _service.GetAllContactUs_IQueryable();
            return View(ObjData);
        }

        public ActionResult Brochure()
        {
            var ObjData = _service.GetAllBrochure_IQueryable();
            return View(ObjData);
        }

        public ActionResult AppliedUser()
        {
            var ObjData = _service.GetAllJobApply_IQueryable();
            return View(ObjData);
        }
    }
}