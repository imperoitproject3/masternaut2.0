$(document).ready(function (e) {
    BindiCheck();
});

function ActiveMenu(liId) {
    $("#li" + liId).addClass("active");
}

function deleteRow(control, Url) {
    control.children('i').removeClass('icon-trash-o').addClass('icon-spinner10 spinner');
    swal({
        title: "Are you sure to delete this record?",
        text: "You will not be able to recover this date!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DA4453",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                $.post(Url, { 'id': $(control).attr('data-id') }, function (response) {
                    console.log(response);
                    if (response.Status == 1) {
                        control.parent().parent().parent().hide(200);
                        setTimeout(function () { control.remove() }, 300);
                        swal("Deleted!", response.Message, "success");
                    }
                    else {
                        control.children('i').removeClass('icon-spinner10 spinner').addClass('icon-trash-o');
                        swal("Error", response.Message, "error");
                    }
                });
            } else {
                control.children('i').removeClass('icon-spinner10 spinner').addClass('icon-trash-o');
                swal("Cancelled", "Your record is safe :)", "info");
            }
        });

}


function StartBlockPage() {
    var block_body = $("#body");
    $(block_body).block({
        message: '<div class="icon-spinner10 spinner spinner--steps2 font-large-5"></div><br /><br /><h2>Please wait while image is being uploaded.</h2>',
        overlayCSS: {
            backgroundColor: '#FFF',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

function CloseBlockPage() {
    var block_body = $("#body");
    $(block_body).unblock();
}

function BindRepeater() {
    $('.repeater').repeater();
}

function BindTouchspin() {
    $(".touchspin").TouchSpin({
        buttondown_class: "btn btn-danger",
        buttonup_class: "btn btn-success",
        buttondown_txt: '<i class="icon-minus4"></i>',
        buttonup_txt: '<i class="icon-plus4"></i>',
        max: 999999,
    });
}

function BindRepeater() {
    $('.repeater').repeater();
}

function BindiCheck() {
    if ($('.chk-icheck').length) {
        $('.chk-icheck').iCheck({
            checkboxClass: 'icheckbox_square-purple',
            radioClass: 'iradio_square-purple',
        });
    }
}
function SetAddressComponents(argID, Latitude, Longitude, EncodeAddress) {
    var Address = Base64Decode(EncodeAddress);
    PrimaryID = argID;
    currentLatitude = Latitude;
    currentLongitude = Longitude;
    CurrentAddress = Address;

    $("#Latitude").val(currentLatitude);
    $("#Longitude").val(currentLongitude);
    //$('#txtAddressSearch').val(CurrentAddress);
}
function BindAddressSearch() {
    var txtAddress = document.getElementsByClassName('googleaddress')[0];
    if (txtAddress) {
        autocompleteAddress = new google.maps.places.Autocomplete((txtAddress), {
            //types: ['establishment'],
            //componentRestrictions: { 'country': 'uk' }
        });
        google.maps.event.addListener(autocompleteAddress, 'place_changed', function () {
            var place = autocompleteAddress.getPlace();
            currentLatitude = place.geometry.location.lat();
            currentLongitude = place.geometry.location.lng();
            var Address = $(txtAddress).val();
            AutoFillAddressGeomatry(Address);
        });
    }
}

//Set Address
{
    function AutoFillAddressGeomatry(Address) {
        var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
        var geocoder = new google.maps.Geocoder();

        //geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        geocoder.geocode({ 'address': Address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results);
                var address_components = results[0].address_components;

                if (Address == "")
                    CurrentAddress = results[0].formatted_address;
                else
                    CurrentAddress = Address;
                var Street = "";
                var City = "";
                var Country = "";
                var Zipcode = "";

                for (var i = 0; i < address_components.length; i++) {
                    var long_name = address_components[i].long_name;

                    var types = address_components[i].types;

                    for (var j = 0; j < types.length; j++) {
                        var addressType = address_components[i].types[j];

                        switch (addressType) {
                            case 'route':
                            case 'sublocality_level_1':
                            case 'sublocality':
                                {
                                    Street = long_name;
                                    break;
                                }

                            case 'postal_town':
                            case 'locality':
                                {
                                    City = long_name;
                                    break;
                                }

                            case 'country':
                            case 'administrative_area_level_1':
                                {
                                    Country = long_name;
                                    break;
                                }

                            case 'postal_code':
                                {
                                    Zipcode = long_name;
                                    break;
                                }
                        }
                    }
                }
                $("#LocationLatitude").val(currentLatitude);
                $('.googleLatitude').val(currentLatitude);
                $("#LocationLongitude").val(currentLongitude);
                $(".googleLongitude").val(currentLongitude);

                //$('#txtAddressSearch').val(CurrentAddress);
               // $(".googleaddress").val(CurrentAddress);
                //$('.googlestreet').val(Street);
                //$('.googlepostcode').val(Zipcode);
                //if (Zipcode)
                //    $('.googlepostcode').val(Zipcode);
                //else
                //    $('.googlepostcode').val(CurrentAddress);
                //$('.googlecity').val(City);
                InitilizeMap();
            }
        });
    }
}