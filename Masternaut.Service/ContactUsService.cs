﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class ContactUsService
    {
        private readonly ContactUsRepository _repo;
        public ContactUsService()
        {
            _repo = new ContactUsRepository();
        }

        public async Task<ResponseModel<object>> SaveContactUs(ContactUsModel model)
        {
            return await _repo.SaveContactUs(model);
        }

        public async Task<ResponseModel<object>> SaveJobApply(JobApplyModel model)
        {
            return await _repo.SaveJobApply(model);
        }
        public IQueryable<ContactUsModel> GetAllContactUs_IQueryable()
        {
            return _repo.GetAllContactUs_IQueryable();
        }

        public IQueryable<ContactUsModel> GetAllBrochure_IQueryable()
        {
            return _repo.GetAllBrochure_IQueryable();
        }
        public IQueryable<JobApplyViewModel> GetAllJobApply_IQueryable()
        {
            return _repo.GetAllJobApply_IQueryable();
        }
    }
}
