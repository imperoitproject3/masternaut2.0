﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class EventService
    {
        private EventRepository _repo;
        public EventService()
        {
            _repo = new EventRepository();
        }

        public IQueryable<EventViewModel> GetAllEvent_IQueryable()
        {
            return _repo.GetAllEvent_IQueryable();
        }
        public async Task<ResponseModel<object>> InsertOrUpdateEvent(EventAddUpdateModel model)
        {
            return await _repo.InsertOrUpdateEvent(model);
        }

        public async Task<EventAddUpdateModel> GetEventDetail(long Id)
        {
            return await _repo.GetEventDetail(Id);
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            return await _repo.Delete(Id);
        }

        public async Task<ViewAllEventModel> GetEventList()
        {
            return await _repo.GetEventList();
        }

        public async Task<int> GetTotalEventCount()
        {
            return await _repo.GetTotalEventCount();
        }

        public async Task<SingleEventViewModel> GetEventDetailsForWeb(long Id)
        {
            return await _repo.GetEventDetailsForWeb(Id);
        }
    }
}
