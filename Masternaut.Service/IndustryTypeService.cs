﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class IndustryTypeService : IDisposable
    {
        private readonly IndustryTypeRepository _repo;

        public IndustryTypeService()
        {
            _repo = new IndustryTypeRepository();
        }

        public IQueryable<DropdownModel> GetIndustryDropdown()
        {
            return _repo.GetIndustryDropdown();
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
