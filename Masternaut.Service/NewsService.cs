﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class NewsService : IDisposable
    {
        private NewsRepository _repo;

        public NewsService()
        {
            _repo = new NewsRepository();
        }

        public IQueryable<NewsViewModel> GetAllNews_IQueryable()
        {
            return _repo.GetAllNews_IQueryable();
        }

        public async Task<ResponseModel<object>> InsertOrUpdateNews(NewsAddUpdateModel model)
        {
            return await _repo.InsertOrUpdateNews(model);
        }

        public async Task<NewsAddUpdateModel> GetNewsDetail(long Id)
        {
            return await _repo.GetNewsDetail(Id);
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            return await _repo.Delete(Id);
        }
        public async Task<List<SingleNewsViewModel>> GetAllNews(PaginationModel model)
        {
            return await _repo.GetAllNews(model);
        }
        public async Task<List<NewsTitle>> GetlatestNews()
        {
            return await _repo.GetlatestNews();
        }
        public async Task<int> GetTotalNewsCount()
        {
            return await _repo.GetTotalNewsCount();
        }

        public async Task<SingleNewsViewModel> GetNewsDetailsForWeb(long Id)
        {
            return await _repo.GetNewsDetailsForWeb(Id);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
