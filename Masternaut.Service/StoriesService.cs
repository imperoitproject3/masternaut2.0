﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class StoriesService : IDisposable
    {
        private StoriesRepository _repo;

        public StoriesService()
        {
            _repo = new StoriesRepository();
        }

        public IQueryable<StoriesViewModel> GetAllStories_IQueryable()
        {
            return _repo.GetAllStories_IQueryable();
        }

        public async Task<ResponseModel<object>> InsertOrUpdateStories(StoriesAddUpdateModel model)
        {
            return await _repo.InsertOrUpdateStories(model);
        }

        public async Task<StoriesAddUpdateModel> GetStoriesDetail(long Id)
        {
            return await _repo.GetStoriesDetail(Id);
        }

        public async Task<ResponseModel<object>> Delete(long Id)
        {
            return await _repo.Delete(Id);
        }

        public async Task<List<SingleStoriesViewModel>> GetTopStories()
        {
            return await _repo.GetTopStories();
        }

        public async Task<CustomerStoriesViewModel> GetAllStories()
        {
            return await _repo.GetAllStories();
        }
        public async Task<SingleStoriesViewModel> GetStoriesDetails(long Id)
        {
            return await _repo.GetStoriesDetails(Id);
        }
            public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
