﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class CategoryService : IDisposable
    {
        private readonly CategoryRepository _repo;

        public CategoryService()
        {
            _repo = new CategoryRepository();
        }

        public IQueryable<DropdownModel> GetCategoryDropdown()
        {
            return _repo.GetCategoryDropdown();
        }
        public IQueryable<AdminCategoryModel> GetAllCategory_IQueryable()
        {
            return _repo.GetAllCategory_IQueryable();
        }

        public bool isCategoryDuplicate(AdminCategoryModel model)
        {
            return _repo.isCategoryDuplicate(model);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateCategory(AdminCategoryModel model)
        {
            return await _repo.InsertOrUpdateCategory(model);
        }

        public async Task<ResponseModel<object>> DeleteCategory(long CategoryId)
        {
            return await _repo.DeleteCategory(CategoryId);
        }

        public async Task<AdminCategoryModel> GetCategoryDetail(long CatrgoryId)
        {
            return await _repo.GetCategoryDetail(CatrgoryId);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
