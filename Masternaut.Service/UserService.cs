﻿using Masternaut.Core.Model;
using Masternaut.Data.Repository;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masternaut.Service
{
    public class UserService : IDisposable
    {
        private UserRepository _repo;
        private readonly IOwinContext _owin;
        public UserService()
        {
            _repo = new UserRepository();
        }

        public UserService(IOwinContext owin)
        {
            _repo = new UserRepository(owin);
            this._owin = owin;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            return await _repo.AdminLogin(model);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            return await _repo.AdminChangePassword(model);
        }

        public void WebLogout()
        {
            _repo.WebLogout();
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
